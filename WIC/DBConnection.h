#pragma once

using namespace System;
using namespace System::Data;
using namespace System::Windows::Forms;
using namespace MySql::Data::MySqlClient;

public ref class DBConnection
{
public:
	DBConnection(String^ server,
						   String^ uname,String^ passwd);
	~DBConnection();
	void Close(void);
	void Insert(String^ table,String ^tuple);
	void Search(String^ table, String^ searchTerm,String^ searchField,DataGridView^ dataGrid);
	void Update(void);
	void RunNonQuery(String^ query);
	String^ Get(String^ table, String^ field, String^ id);
	cli::array<String^> ^Get(String^ table, String ^idName,String^ id,int size);
	String ^GetID(String^ table,String^ idName);
	void Search(DataGridView^ dataGrid,String^ table, String^ condition,String^ values);

private:
	MySqlConnection^ myConnection;
	DataTable^ data;
	MySqlDataAdapter^ da;
	MySqlCommandBuilder^ cb;
};
