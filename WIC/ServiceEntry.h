#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

#include "DBConnection.h"

namespace WIC {

	/// <summary>
	/// Summary for ServiceEntry
	///
	/// WARNING: If you change the name of this class, you will need to change the
	///          'Resource File Name' property for the managed resource compiler tool
	///          associated with all .resx files this class depends on.  Otherwise,
	///          the designers will not be able to interact properly with localized
	///          resources associated with this form.
	/// </summary>
	public ref class ServiceEntry : public System::Windows::Forms::Form
	{
	public:
		ServiceEntry(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~ServiceEntry()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::GroupBox^  groupBox1;
	private: System::Windows::Forms::GroupBox^  groupBox2;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::TextBox^  commentsText;




	private: System::Windows::Forms::TextBox^  typeText;
	private: System::Windows::Forms::TextBox^  nameText;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::DateTimePicker^  dobField;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::DateTimePicker^  dateField;
	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::ComboBox^  entryCombo;
	private: System::Windows::Forms::Label^  label7;
	private: System::Windows::Forms::Button^  cancelButton;

	private: System::Windows::Forms::Button^  okButton;

	protected: 

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->dobField = (gcnew System::Windows::Forms::DateTimePicker());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->typeText = (gcnew System::Windows::Forms::TextBox());
			this->nameText = (gcnew System::Windows::Forms::TextBox());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->cancelButton = (gcnew System::Windows::Forms::Button());
			this->okButton = (gcnew System::Windows::Forms::Button());
			this->entryCombo = (gcnew System::Windows::Forms::ComboBox());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->dateField = (gcnew System::Windows::Forms::DateTimePicker());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->commentsText = (gcnew System::Windows::Forms::TextBox());
			this->groupBox1->SuspendLayout();
			this->groupBox2->SuspendLayout();
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 18, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->label1->Location = System::Drawing::Point(24, 40);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(0, 29);
			this->label1->TabIndex = 0;
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->dobField);
			this->groupBox1->Controls->Add(this->label5);
			this->groupBox1->Controls->Add(this->typeText);
			this->groupBox1->Controls->Add(this->nameText);
			this->groupBox1->Controls->Add(this->label4);
			this->groupBox1->Controls->Add(this->label3);
			this->groupBox1->Controls->Add(this->label1);
			this->groupBox1->Location = System::Drawing::Point(12, 12);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(334, 84);
			this->groupBox1->TabIndex = 1;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"Participant Info";
			// 
			// dobField
			// 
			this->dobField->CustomFormat = L"yyyy-MM-dd";
			this->dobField->Enabled = false;
			this->dobField->Format = System::Windows::Forms::DateTimePickerFormat::Custom;
			this->dobField->Location = System::Drawing::Point(81, 51);
			this->dobField->Name = L"dobField";
			this->dobField->Size = System::Drawing::Size(91, 20);
			this->dobField->TabIndex = 3;
			this->dobField->Value = System::DateTime(2006, 5, 3, 0, 0, 0, 0);
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(6, 55);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(69, 13);
			this->label5->TabIndex = 6;
			this->label5->Text = L"Date of Birth:";
			// 
			// typeText
			// 
			this->typeText->Location = System::Drawing::Point(227, 17);
			this->typeText->Name = L"typeText";
			this->typeText->ReadOnly = true;
			this->typeText->Size = System::Drawing::Size(65, 20);
			this->typeText->TabIndex = 2;
			// 
			// nameText
			// 
			this->nameText->Location = System::Drawing::Point(53, 17);
			this->nameText->Name = L"nameText";
			this->nameText->ReadOnly = true;
			this->nameText->Size = System::Drawing::Size(119, 20);
			this->nameText->TabIndex = 1;
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(9, 20);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(38, 13);
			this->label4->TabIndex = 3;
			this->label4->Text = L"Name:";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(187, 20);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(34, 13);
			this->label3->TabIndex = 2;
			this->label3->Text = L"Type:";
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->cancelButton);
			this->groupBox2->Controls->Add(this->okButton);
			this->groupBox2->Controls->Add(this->entryCombo);
			this->groupBox2->Controls->Add(this->label7);
			this->groupBox2->Controls->Add(this->label6);
			this->groupBox2->Controls->Add(this->dateField);
			this->groupBox2->Controls->Add(this->label2);
			this->groupBox2->Controls->Add(this->commentsText);
			this->groupBox2->Location = System::Drawing::Point(12, 102);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Size = System::Drawing::Size(333, 240);
			this->groupBox2->TabIndex = 2;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"Service Entry";
			// 
			// cancelButton
			// 
			this->cancelButton->Location = System::Drawing::Point(190, 188);
			this->cancelButton->Name = L"cancelButton";
			this->cancelButton->Size = System::Drawing::Size(130, 42);
			this->cancelButton->TabIndex = 8;
			this->cancelButton->Text = L"Cancel";
			this->cancelButton->UseVisualStyleBackColor = true;
			this->cancelButton->Click += gcnew System::EventHandler(this, &ServiceEntry::cancelButton_Click);
			// 
			// okButton
			// 
			this->okButton->Location = System::Drawing::Point(14, 188);
			this->okButton->Name = L"okButton";
			this->okButton->Size = System::Drawing::Size(130, 42);
			this->okButton->TabIndex = 7;
			this->okButton->Text = L"OK";
			this->okButton->UseVisualStyleBackColor = true;
			this->okButton->Click += gcnew System::EventHandler(this, &ServiceEntry::okButton_Click);
			// 
			// entryCombo
			// 
			this->entryCombo->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->entryCombo->FormattingEnabled = true;
			this->entryCombo->Items->AddRange(gcnew cli::array< System::Object^  >(9) {L"WCI � WIC Certification Infant", L"WCC1 � WIC Certification Child Under 2", 
				L"WCC2 � WIC Certification Child Over 2", L"WCB � WIC Certification Breast Feeding", L"WCM � WIC Certification Postpartum", L"WEC1 � WIC Nutrition Education Child Under 2", 
				L"WEC2 � WIC Nutrition Education Child Over 2", L"WFIO � WIC Food Instruments Only", L"WIP � WIC Individual Education"});
			this->entryCombo->Location = System::Drawing::Point(73, 53);
			this->entryCombo->Name = L"entryCombo";
			this->entryCombo->Size = System::Drawing::Size(254, 21);
			this->entryCombo->TabIndex = 5;
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(6, 56);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(61, 13);
			this->label7->TabIndex = 10;
			this->label7->Text = L"Entry Type:";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(6, 23);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(72, 13);
			this->label6->TabIndex = 9;
			this->label6->Text = L"Service Date:";
			// 
			// dateField
			// 
			this->dateField->CustomFormat = L"yyyy-MM-dd";
			this->dateField->Format = System::Windows::Forms::DateTimePickerFormat::Custom;
			this->dateField->Location = System::Drawing::Point(84, 19);
			this->dateField->Name = L"dateField";
			this->dateField->Size = System::Drawing::Size(88, 20);
			this->dateField->TabIndex = 4;
			this->dateField->Value = System::DateTime(2006, 5, 3, 0, 0, 0, 0);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(12, 85);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(56, 13);
			this->label2->TabIndex = 1;
			this->label2->Text = L"Comments";
			// 
			// commentsText
			// 
			this->commentsText->AcceptsReturn = true;
			this->commentsText->Location = System::Drawing::Point(6, 101);
			this->commentsText->Multiline = true;
			this->commentsText->Name = L"commentsText";
			this->commentsText->Size = System::Drawing::Size(321, 81);
			this->commentsText->TabIndex = 6;
			// 
			// ServiceEntry
			// 
			this->AcceptButton = this->okButton;
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->CancelButton = this->cancelButton;
			this->ClientSize = System::Drawing::Size(358, 354);
			this->Controls->Add(this->groupBox2);
			this->Controls->Add(this->groupBox1);
			this->MaximizeBox = false;
			this->Name = L"ServiceEntry";
			this->ShowIcon = false;
			this->Text = L"Service Entry";
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->groupBox2->ResumeLayout(false);
			this->groupBox2->PerformLayout();
			this->ResumeLayout(false);

		}
#pragma endregion
public:	void SetText(System::String ^ text) {
			label1->Text = text;
		}
public: DBConnection^ dbconn;
private: String^ id;
private: static int sid = 0;
public: bool update;
public: System::Void Update(String^ userID, String^ name, String^ type, Windows::Forms::DateTimePicker ^ dob){
			this->id = userID;
			this->nameText->Text = name;
			this->typeText->Text = type;
			this->dobField->Value = dob->Value;
		}
private: System::Void okButton_Click(System::Object^  sender, System::EventArgs^  e) {
			 String ^newSid;
			 newSid = dbconn->GetID("ServiceEntry","EntryID");
			 sid = Int32::Parse(newSid) + 1;	 
			 dbconn->Insert("ServiceEntry","('"+ sid +"','" + this->dateField->Text + "'," + 
				 (this->entryCombo->SelectedIndex + 1) + ",'" + this->commentsText->Text + "')");
			 if(this->typeText->Text == "Mother" || this->typeText->Text == "Adult")
				 dbconn->Insert("AdultReceives","('"+ sid +"','"+ id +"')");
			 else
				 dbconn->Insert("ChildReceives","('"+ sid +"','"+ id +"')");
			 sid++;
			 this->Close();
		 }
private: System::Void cancelButton_Click(System::Object^  sender, System::EventArgs^  e) {
			 this->Close();
		 }
};
}
