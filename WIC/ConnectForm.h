#pragma once
#include "DBConnection.h"

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;


namespace WIC {

	/// <summary>
	/// Summary for ConnectForm
	///
	/// WARNING: If you change the name of this class, you will need to change the
	///          'Resource File Name' property for the managed resource compiler tool
	///          associated with all .resx files this class depends on.  Otherwise,
	///          the designers will not be able to interact properly with localized
	///          resources associated with this form.
	/// </summary>
	public ref class ConnectForm : public System::Windows::Forms::Form
	{
	public:
		ConnectForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~ConnectForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::TextBox^  serverText;
	protected: 
	private: System::Windows::Forms::TextBox^  userText;
	private: System::Windows::Forms::TextBox^  passText;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Button^  connButton;
	private: System::Windows::Forms::Button^  cancelButton;



	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->serverText = (gcnew System::Windows::Forms::TextBox());
			this->userText = (gcnew System::Windows::Forms::TextBox());
			this->passText = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->connButton = (gcnew System::Windows::Forms::Button());
			this->cancelButton = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// serverText
			// 
			this->serverText->Location = System::Drawing::Point(108, 12);
			this->serverText->Name = L"serverText";
			this->serverText->Size = System::Drawing::Size(225, 20);
			this->serverText->TabIndex = 0;
			this->serverText->Text = L"64.198.231.175";
			// 
			// userText
			// 
			this->userText->Location = System::Drawing::Point(81, 38);
			this->userText->MaxLength = 20;
			this->userText->Name = L"userText";
			this->userText->Size = System::Drawing::Size(88, 20);
			this->userText->TabIndex = 1;
			this->userText->Text = L"cs411";
			// 
			// passText
			// 
			this->passText->Location = System::Drawing::Point(237, 38);
			this->passText->MaxLength = 20;
			this->passText->Name = L"passText";
			this->passText->Size = System::Drawing::Size(96, 20);
			this->passText->TabIndex = 2;
			this->passText->Text = L"posse";
			this->passText->UseSystemPasswordChar = true;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(12, 15);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(90, 13);
			this->label1->TabIndex = 3;
			this->label1->Text = L"Database Server:";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(12, 41);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(63, 13);
			this->label2->TabIndex = 4;
			this->label2->Text = L"User Name:";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(175, 41);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(56, 13);
			this->label3->TabIndex = 5;
			this->label3->Text = L"Password:";
			// 
			// connButton
			// 
			this->connButton->Location = System::Drawing::Point(177, 66);
			this->connButton->Name = L"connButton";
			this->connButton->Size = System::Drawing::Size(75, 23);
			this->connButton->TabIndex = 3;
			this->connButton->Text = L"Connect";
			this->connButton->UseVisualStyleBackColor = true;
			this->connButton->Click += gcnew System::EventHandler(this, &ConnectForm::connButton_Click);
			// 
			// cancelButton
			// 
			this->cancelButton->DialogResult = System::Windows::Forms::DialogResult::Cancel;
			this->cancelButton->Location = System::Drawing::Point(258, 66);
			this->cancelButton->Name = L"cancelButton";
			this->cancelButton->Size = System::Drawing::Size(75, 23);
			this->cancelButton->TabIndex = 4;
			this->cancelButton->Text = L"Cancel";
			this->cancelButton->UseVisualStyleBackColor = true;
			this->cancelButton->Click += gcnew System::EventHandler(this, &ConnectForm::cancelButton_Click);
			// 
			// ConnectForm
			// 
			this->AcceptButton = this->connButton;
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->CancelButton = this->cancelButton;
			this->ClientSize = System::Drawing::Size(345, 101);
			this->Controls->Add(this->cancelButton);
			this->Controls->Add(this->connButton);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->passText);
			this->Controls->Add(this->userText);
			this->Controls->Add(this->serverText);
			this->Name = L"ConnectForm";
			this->ShowIcon = false;
			this->Text = L"Connect";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	public: DBConnection^	dbconn;
	private: System::Void connButton_Click(System::Object^  sender, System::EventArgs^  e) {
				 dbconn = (gcnew DBConnection(this->serverText->Text,
					 this->userText->Text,this->passText->Text));
				 this->Close();
			 }
private: System::Void cancelButton_Click(System::Object^  sender, System::EventArgs^  e) {
			 this->Close();
		 }
};
}
