#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

#include "DBConnection.h"

namespace WIC {

	/// <summary>
	/// Summary for ServiceHistory
	///
	/// WARNING: If you change the name of this class, you will need to change the
	///          'Resource File Name' property for the managed resource compiler tool
	///          associated with all .resx files this class depends on.  Otherwise,
	///          the designers will not be able to interact properly with localized
	///          resources associated with this form.
	/// </summary>
	public ref class ServiceHistory : public System::Windows::Forms::Form
	{
	public:
		ServiceHistory(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~ServiceHistory()
		{
			if (components)
			{
				delete components;
			}
		}
	public: System::Windows::Forms::DataGridView^  searchView;
	protected: 

	protected: 

	protected: 

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->searchView = (gcnew System::Windows::Forms::DataGridView());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->searchView))->BeginInit();
			this->SuspendLayout();
			// 
			// searchView
			// 
			this->searchView->AllowUserToAddRows = false;
			this->searchView->AllowUserToOrderColumns = true;
			this->searchView->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
				| System::Windows::Forms::AnchorStyles::Left) 
				| System::Windows::Forms::AnchorStyles::Right));
			this->searchView->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::AllCells;
			this->searchView->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->searchView->Location = System::Drawing::Point(12, 12);
			this->searchView->Name = L"searchView";
			this->searchView->ReadOnly = true;
			this->searchView->SelectionMode = System::Windows::Forms::DataGridViewSelectionMode::FullRowSelect;
			this->searchView->Size = System::Drawing::Size(501, 377);
			this->searchView->TabIndex = 0;
			this->searchView->UserDeletingRow += gcnew System::Windows::Forms::DataGridViewRowCancelEventHandler(this, &ServiceHistory::searchView_UserDeletingRow);
			this->searchView->UserDeletedRow += gcnew System::Windows::Forms::DataGridViewRowEventHandler(this, &ServiceHistory::searchView_UserDeletedRow);
			// 
			// ServiceHistory
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(525, 401);
			this->Controls->Add(this->searchView);
			this->Name = L"ServiceHistory";
			this->Text = L"ServiceHistory";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->searchView))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
	public: DBConnection^ dbconn;
	private: System::Void searchView_UserDeletingRow(System::Object^  sender, System::Windows::Forms::DataGridViewRowCancelEventArgs^  e) {
			 System::Windows::Forms::DialogResult result;
			 result = MessageBox::Show("Are you sure you would like to delete this row?","Delete?",
			 MessageBoxButtons::YesNo,MessageBoxIcon::Question,MessageBoxDefaultButton::Button1);
			 if(result == System::Windows::Forms::DialogResult::No)
				 e->Cancel = true;
			 }
	private: System::Void searchView_UserDeletedRow(System::Object^  sender, System::Windows::Forms::DataGridViewRowEventArgs^  e) {
				 dbconn->Update();
			 }
	};
}
