#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

#include "ParticipantEnrollment.h"
#include "DBConnection.h"

//Added by Jenkins
#include "PartOptions.h"


namespace WIC {

	/// <summary>
	/// Summary for PartSearch
	///
	/// WARNING: If you change the name of this class, you will need to change the
	///          'Resource File Name' property for the managed resource compiler tool
	///          associated with all .resx files this class depends on.  Otherwise,
	///          the designers will not be able to interact properly with localized
	///          resources associated with this form.
	/// </summary>
	public ref class PartSearch : public System::Windows::Forms::Form
	{
	public:
		PartSearch(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~PartSearch()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::DataGridView^  dataGridView1;
	private: System::Windows::Forms::TextBox^  searchText;
	protected: 

	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::ComboBox^  searchCombo;

	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Button^  searchButton;

	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::ComboBox^  typeCombo;
	private: System::Windows::Forms::DateTimePicker^  dobField;
	private: System::ComponentModel::IContainer^  components;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->dataGridView1 = (gcnew System::Windows::Forms::DataGridView());
			this->searchText = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->searchCombo = (gcnew System::Windows::Forms::ComboBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->searchButton = (gcnew System::Windows::Forms::Button());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->typeCombo = (gcnew System::Windows::Forms::ComboBox());
			this->dobField = (gcnew System::Windows::Forms::DateTimePicker());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dataGridView1))->BeginInit();
			this->SuspendLayout();
			// 
			// dataGridView1
			// 
			this->dataGridView1->AllowUserToAddRows = false;
			this->dataGridView1->AllowUserToOrderColumns = true;
			this->dataGridView1->AllowUserToResizeRows = false;
			this->dataGridView1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
				| System::Windows::Forms::AnchorStyles::Left) 
				| System::Windows::Forms::AnchorStyles::Right));
			this->dataGridView1->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::AllCells;
			this->dataGridView1->AutoSizeRowsMode = System::Windows::Forms::DataGridViewAutoSizeRowsMode::AllCells;
			this->dataGridView1->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dataGridView1->Location = System::Drawing::Point(12, 70);
			this->dataGridView1->Name = L"dataGridView1";
			this->dataGridView1->ReadOnly = true;
			this->dataGridView1->SelectionMode = System::Windows::Forms::DataGridViewSelectionMode::FullRowSelect;
			this->dataGridView1->Size = System::Drawing::Size(713, 369);
			this->dataGridView1->TabIndex = 6;
			this->dataGridView1->UserDeletingRow += gcnew System::Windows::Forms::DataGridViewRowCancelEventHandler(this, &PartSearch::dataGridView1_UserDeletingRow);
			this->dataGridView1->CellDoubleClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &PartSearch::dataGridView1_CellDoubleClick);
			this->dataGridView1->UserDeletedRow += gcnew System::Windows::Forms::DataGridViewRowEventHandler(this, &PartSearch::dataGridView1_UserDeletedRow);
			this->dataGridView1->RowHeaderMouseDoubleClick += gcnew System::Windows::Forms::DataGridViewCellMouseEventHandler(this, &PartSearch::dataGridView1_RowHeaderMouseDoubleClick);
			// 
			// searchText
			// 
			this->searchText->Enabled = false;
			this->searchText->Location = System::Drawing::Point(74, 44);
			this->searchText->Name = L"searchText";
			this->searchText->Size = System::Drawing::Size(174, 20);
			this->searchText->TabIndex = 3;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(9, 47);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(59, 13);
			this->label1->TabIndex = 2;
			this->label1->Text = L"Search for:";
			// 
			// searchCombo
			// 
			this->searchCombo->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->searchCombo->Enabled = false;
			this->searchCombo->FormattingEnabled = true;
			this->searchCombo->Location = System::Drawing::Point(229, 17);
			this->searchCombo->Name = L"searchCombo";
			this->searchCombo->Size = System::Drawing::Size(141, 21);
			this->searchCombo->TabIndex = 1;
			this->searchCombo->SelectedIndexChanged += gcnew System::EventHandler(this, &PartSearch::searchCombo_SelectedIndexChanged);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(165, 20);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(58, 13);
			this->label2->TabIndex = 4;
			this->label2->Text = L"Search by:";
			// 
			// searchButton
			// 
			this->searchButton->Enabled = false;
			this->searchButton->Location = System::Drawing::Point(254, 44);
			this->searchButton->Name = L"searchButton";
			this->searchButton->Size = System::Drawing::Size(116, 21);
			this->searchButton->TabIndex = 4;
			this->searchButton->Text = L"Search";
			this->searchButton->UseVisualStyleBackColor = true;
			this->searchButton->Click += gcnew System::EventHandler(this, &PartSearch::searchButton_Click);
			// 
			// button1
			// 
			this->button1->Enabled = false;
			this->button1->Location = System::Drawing::Point(376, 44);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(94, 21);
			this->button1->TabIndex = 5;
			this->button1->Text = L"Save Changes";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Visible = false;
			this->button1->Click += gcnew System::EventHandler(this, &PartSearch::button1_Click);
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(34, 20);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(34, 13);
			this->label3->TabIndex = 6;
			this->label3->Text = L"Type:";
			// 
			// typeCombo
			// 
			this->typeCombo->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->typeCombo->FormattingEnabled = true;
			this->typeCombo->Items->AddRange(gcnew cli::array< System::Object^  >(2) {L"Mother", L"Child"});
			this->typeCombo->Location = System::Drawing::Point(74, 17);
			this->typeCombo->Name = L"typeCombo";
			this->typeCombo->Size = System::Drawing::Size(85, 21);
			this->typeCombo->TabIndex = 0;
			this->typeCombo->SelectedIndexChanged += gcnew System::EventHandler(this, &PartSearch::typeCombo_SelectedIndexChanged);
			// 
			// dobField
			// 
			this->dobField->CustomFormat = L"yyyy-MM-dd";
			this->dobField->Enabled = false;
			this->dobField->Format = System::Windows::Forms::DateTimePickerFormat::Custom;
			this->dobField->Location = System::Drawing::Point(74, 44);
			this->dobField->MaxDate = System::DateTime(2010, 4, 16, 0, 0, 0, 0);
			this->dobField->Name = L"dobField";
			this->dobField->Size = System::Drawing::Size(174, 20);
			this->dobField->TabIndex = 2;
			this->dobField->Value = System::DateTime(2006, 1, 1, 0, 0, 0, 0);
			this->dobField->Visible = false;
			// 
			// PartSearch
			// 
			this->AcceptButton = this->searchButton;
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(737, 451);
			this->Controls->Add(this->dobField);
			this->Controls->Add(this->typeCombo);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->searchButton);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->searchCombo);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->searchText);
			this->Controls->Add(this->dataGridView1);
			this->Name = L"PartSearch";
			this->ShowIcon = false;
			this->Text = L"Participant Search";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dataGridView1))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	public: DBConnection^ dbconn;
	private: PartOptions^ partOpts;
	private: System::Void searchButton_Click(System::Object^  sender, System::EventArgs^  e) {
				 String^ tmp;
				 if(this->typeCombo->Text == "Mother")
					 tmp = "Adult";
				 else
					 tmp = "Child";
				 if(this->dobField->Visible==true)
					 dbconn->Search(tmp, this->dobField->Text,this->searchCombo->Text,this->dataGridView1);
				 else if(this->searchText->Visible==true)
					 dbconn->Search(tmp, this->searchText->Text,this->searchCombo->Text,this->dataGridView1);
				 this->dataGridView1->Update();
			 }
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
			dbconn->Update();
		 }
private: System::Void searchCombo_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
			 searchButton->Enabled = true;
			 button1->Enabled = true;
			 searchText->Enabled = true;
			 dobField->Enabled = true;
			 this->dataGridView1->DataSource = nullptr;
			 if(this->searchCombo->Text=="DateOfBirth")
			 {
				 this->dobField->Visible = true;
				 this->searchText->Visible = false;
			 }
			 else
			 {
				 this->dobField->Visible = false;
				 this->searchText->Visible = true;
			 }
		 }
private: System::Void typeCombo_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
			 this->searchCombo->Enabled=true;
			 this->searchButton->Enabled=false;
			 this->button1->Enabled=false;
			 this->dataGridView1->DataSource = nullptr;
			 if(this->typeCombo->Text=="Mother")
			 {
				 this->searchCombo->Items->Clear();
				 this->searchCombo->Items->AddRange(gcnew cli::array< System::Object^  >(4) {L"Name", L"Address", L"DateOfBirth", L"AssessmentID"});
			 }
			 if(this->typeCombo->Text=="Child")
			 {
				 this->searchCombo->Items->Clear();
				 this->searchCombo->Items->AddRange(gcnew cli::array< System::Object^  >(5) {L"Name", L"Address", L"DateOfBirth", L"AssessmentID", L"ParentID"});
			 }
		 }
private: System::Void dataGridView1_UserDeletingRow(System::Object^  sender, System::Windows::Forms::DataGridViewRowCancelEventArgs^  e) {
			 System::Windows::Forms::DialogResult result;
			 result = MessageBox::Show("Are you sure you would like to delete this row?","Delete?",
				 MessageBoxButtons::YesNo,MessageBoxIcon::Question,MessageBoxDefaultButton::Button1);
			 if(result == System::Windows::Forms::DialogResult::No)
				 e->Cancel = true;
		 }
private: System::Void dataGridView1_RowHeaderMouseDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e) {
			 Windows::Forms::DataGridViewRow^ selectedRow;
			 selectedRow = dataGridView1->CurrentRow;
			 if(!partOpts) {
				partOpts = (gcnew PartOptions());
				partOpts->MdiParent = this->MdiParent;
				partOpts->dbconn = this->dbconn;
				partOpts->FormClosed += gcnew System::Windows::Forms::FormClosedEventHandler(this, &PartSearch::optionsWindowClosed); 
				partOpts->Update(selectedRow->Cells->GetEnumerator(),this->typeCombo->Text);
				partOpts->Show();
			 }
			 
		 }
private: System::Void dataGridView1_UserDeletedRow(System::Object^  sender, System::Windows::Forms::DataGridViewRowEventArgs^  e) {
			 dbconn->Update();
		 }
private: System::Void optionsWindowClosed(System::Object^  sender, System::Windows::Forms::FormClosedEventArgs^  e) {
			 partOpts = nullptr;
		 }
private: System::Void dataGridView1_CellDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e) {
			 this->dataGridView1_RowHeaderMouseDoubleClick(sender,nullptr);
		 }
};
}
