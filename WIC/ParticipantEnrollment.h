#pragma once

#include "DBConnection.h"

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;


namespace WIC {

	/// <summary>
	/// Summary for ParticipantEnrollment
	///
	/// WARNING: If you change the name of this class, you will need to change the
	///          'Resource File Name' property for the managed resource compiler tool
	///          associated with all .resx files this class depends on.  Otherwise,
	///          the designers will not be able to interact properly with localized
	///          resources associated with this form.
	/// </summary>
	public ref class ParticipantEnrollment : public System::Windows::Forms::Form
	{
	public:
		ParticipantEnrollment(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~ParticipantEnrollment()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::GroupBox^  groupBox1;
	protected: 

	public: System::Windows::Forms::TextBox^  nameText;
	private: 


	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label8;

	private: System::Windows::Forms::Label^  label7;
	public: System::Windows::Forms::TextBox^  addressText;
	private: 


	private: System::Windows::Forms::Label^  label6;

	private: System::Windows::Forms::Label^  label5;

	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::TextBox^  schoolText;




	private: System::Windows::Forms::Label^  label2;
	public: System::Windows::Forms::ComboBox^  raceCombo;
	private: 


	private: System::Windows::Forms::ComboBox^  maritalCombo;



	private: System::Windows::Forms::Label^  label10;
	public: System::Windows::Forms::TextBox^  disabilitiesText;
	private: 


	private: System::Windows::Forms::Label^  label9;
	private: System::Windows::Forms::TextBox^  languageText;
	public: System::Windows::Forms::ComboBox^  typeCombo;
	private: 


	private: System::Windows::Forms::ComboBox^  employmentCombo;
	private: System::Windows::Forms::Label^  label11;
	public: System::Windows::Forms::Button^  okButton;
	private: 


	private: System::Windows::Forms::Button^  button2;
	public: System::Windows::Forms::DateTimePicker^  dobField;
	private: 




















	protected: 

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->dobField = (gcnew System::Windows::Forms::DateTimePicker());
			this->label11 = (gcnew System::Windows::Forms::Label());
			this->employmentCombo = (gcnew System::Windows::Forms::ComboBox());
			this->typeCombo = (gcnew System::Windows::Forms::ComboBox());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label10 = (gcnew System::Windows::Forms::Label());
			this->disabilitiesText = (gcnew System::Windows::Forms::TextBox());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->languageText = (gcnew System::Windows::Forms::TextBox());
			this->maritalCombo = (gcnew System::Windows::Forms::ComboBox());
			this->raceCombo = (gcnew System::Windows::Forms::ComboBox());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->addressText = (gcnew System::Windows::Forms::TextBox());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->schoolText = (gcnew System::Windows::Forms::TextBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->nameText = (gcnew System::Windows::Forms::TextBox());
			this->okButton = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->groupBox1->SuspendLayout();
			this->SuspendLayout();
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->dobField);
			this->groupBox1->Controls->Add(this->label11);
			this->groupBox1->Controls->Add(this->employmentCombo);
			this->groupBox1->Controls->Add(this->typeCombo);
			this->groupBox1->Controls->Add(this->label5);
			this->groupBox1->Controls->Add(this->label10);
			this->groupBox1->Controls->Add(this->disabilitiesText);
			this->groupBox1->Controls->Add(this->label9);
			this->groupBox1->Controls->Add(this->languageText);
			this->groupBox1->Controls->Add(this->maritalCombo);
			this->groupBox1->Controls->Add(this->raceCombo);
			this->groupBox1->Controls->Add(this->label8);
			this->groupBox1->Controls->Add(this->label7);
			this->groupBox1->Controls->Add(this->addressText);
			this->groupBox1->Controls->Add(this->label6);
			this->groupBox1->Controls->Add(this->label4);
			this->groupBox1->Controls->Add(this->schoolText);
			this->groupBox1->Controls->Add(this->label2);
			this->groupBox1->Controls->Add(this->label1);
			this->groupBox1->Controls->Add(this->nameText);
			this->groupBox1->Location = System::Drawing::Point(12, 12);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(433, 211);
			this->groupBox1->TabIndex = 0;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"Participant Info";
			// 
			// dobField
			// 
			this->dobField->CustomFormat = L"yyyy-MM-dd";
			this->dobField->Enabled = false;
			this->dobField->Format = System::Windows::Forms::DateTimePickerFormat::Custom;
			this->dobField->Location = System::Drawing::Point(295, 45);
			this->dobField->MaxDate = System::DateTime(2010, 4, 16, 0, 0, 0, 0);
			this->dobField->Name = L"dobField";
			this->dobField->Size = System::Drawing::Size(132, 20);
			this->dobField->TabIndex = 2;
			this->dobField->Value = System::DateTime(2006, 1, 1, 0, 0, 0, 0);
			// 
			// label11
			// 
			this->label11->AutoSize = true;
			this->label11->Location = System::Drawing::Point(113, 21);
			this->label11->Name = L"label11";
			this->label11->Size = System::Drawing::Size(34, 13);
			this->label11->TabIndex = 22;
			this->label11->Text = L"Type:";
			// 
			// employmentCombo
			// 
			this->employmentCombo->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->employmentCombo->Enabled = false;
			this->employmentCombo->FormattingEnabled = true;
			this->employmentCombo->Items->AddRange(gcnew cli::array< System::Object^  >(2) {L"Employed", L"Unemployed"});
			this->employmentCombo->Location = System::Drawing::Point(326, 98);
			this->employmentCombo->Name = L"employmentCombo";
			this->employmentCombo->Size = System::Drawing::Size(101, 21);
			this->employmentCombo->TabIndex = 5;
			// 
			// typeCombo
			// 
			this->typeCombo->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->typeCombo->FormattingEnabled = true;
			this->typeCombo->Items->AddRange(gcnew cli::array< System::Object^  >(2) {L"Mother", L"Child"});
			this->typeCombo->Location = System::Drawing::Point(153, 18);
			this->typeCombo->Name = L"typeCombo";
			this->typeCombo->Size = System::Drawing::Size(165, 21);
			this->typeCombo->TabIndex = 0;
			this->typeCombo->SelectionChangeCommitted += gcnew System::EventHandler(this, &ParticipantEnrollment::typeCombo_SelectionChangeCommitted);
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(220, 101);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(100, 13);
			this->label5->TabIndex = 9;
			this->label5->Text = L"Employment Status:";
			// 
			// label10
			// 
			this->label10->AutoSize = true;
			this->label10->Location = System::Drawing::Point(6, 154);
			this->label10->Name = L"label10";
			this->label10->Size = System::Drawing::Size(59, 13);
			this->label10->TabIndex = 19;
			this->label10->Text = L"Disabilities:";
			// 
			// disabilitiesText
			// 
			this->disabilitiesText->Enabled = false;
			this->disabilitiesText->Location = System::Drawing::Point(71, 150);
			this->disabilitiesText->MaxLength = 500;
			this->disabilitiesText->Multiline = true;
			this->disabilitiesText->Name = L"disabilitiesText";
			this->disabilitiesText->Size = System::Drawing::Size(143, 48);
			this->disabilitiesText->TabIndex = 8;
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Location = System::Drawing::Point(220, 154);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(63, 13);
			this->label9->TabIndex = 17;
			this->label9->Text = L"Languages:";
			// 
			// languageText
			// 
			this->languageText->Enabled = false;
			this->languageText->Location = System::Drawing::Point(289, 152);
			this->languageText->MaxLength = 500;
			this->languageText->Multiline = true;
			this->languageText->Name = L"languageText";
			this->languageText->Size = System::Drawing::Size(138, 46);
			this->languageText->TabIndex = 9;
			// 
			// maritalCombo
			// 
			this->maritalCombo->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->maritalCombo->Enabled = false;
			this->maritalCombo->FormattingEnabled = true;
			this->maritalCombo->Items->AddRange(gcnew cli::array< System::Object^  >(3) {L"Single", L"Married", L"Divorced/Separated"});
			this->maritalCombo->Location = System::Drawing::Point(295, 125);
			this->maritalCombo->Name = L"maritalCombo";
			this->maritalCombo->Size = System::Drawing::Size(132, 21);
			this->maritalCombo->TabIndex = 7;
			// 
			// raceCombo
			// 
			this->raceCombo->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->raceCombo->Enabled = false;
			this->raceCombo->FormattingEnabled = true;
			this->raceCombo->Items->AddRange(gcnew cli::array< System::Object^  >(5) {L"White/Caucasian", L"African American", L"Hispanic/Latino", 
				L"Asian/Pacific Islander", L"Other"});
			this->raceCombo->Location = System::Drawing::Point(262, 71);
			this->raceCombo->Name = L"raceCombo";
			this->raceCombo->Size = System::Drawing::Size(165, 21);
			this->raceCombo->TabIndex = 4;
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Location = System::Drawing::Point(220, 127);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(74, 13);
			this->label8->TabIndex = 15;
			this->label8->Text = L"Marital Status:";
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(6, 74);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(48, 13);
			this->label7->TabIndex = 13;
			this->label7->Text = L"Address:";
			// 
			// addressText
			// 
			this->addressText->AcceptsReturn = true;
			this->addressText->Enabled = false;
			this->addressText->Location = System::Drawing::Point(60, 71);
			this->addressText->Multiline = true;
			this->addressText->Name = L"addressText";
			this->addressText->Size = System::Drawing::Size(154, 47);
			this->addressText->TabIndex = 3;
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(220, 74);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(36, 13);
			this->label6->TabIndex = 11;
			this->label6->Text = L"Race:";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(6, 127);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(85, 13);
			this->label4->TabIndex = 7;
			this->label4->Text = L"Years of School:";
			// 
			// schoolText
			// 
			this->schoolText->Enabled = false;
			this->schoolText->Location = System::Drawing::Point(97, 124);
			this->schoolText->MaxLength = 2;
			this->schoolText->Name = L"schoolText";
			this->schoolText->Size = System::Drawing::Size(116, 20);
			this->schoolText->TabIndex = 6;
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(220, 49);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(69, 13);
			this->label2->TabIndex = 3;
			this->label2->Text = L"Date of Birth:";
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(6, 48);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(38, 13);
			this->label1->TabIndex = 1;
			this->label1->Text = L"Name:";
			// 
			// nameText
			// 
			this->nameText->Enabled = false;
			this->nameText->Location = System::Drawing::Point(60, 45);
			this->nameText->MaxLength = 40;
			this->nameText->Name = L"nameText";
			this->nameText->Size = System::Drawing::Size(154, 20);
			this->nameText->TabIndex = 1;
			// 
			// okButton
			// 
			this->okButton->Location = System::Drawing::Point(259, 229);
			this->okButton->Name = L"okButton";
			this->okButton->Size = System::Drawing::Size(90, 24);
			this->okButton->TabIndex = 14;
			this->okButton->Text = L"Save";
			this->okButton->UseVisualStyleBackColor = true;
			this->okButton->Click += gcnew System::EventHandler(this, &ParticipantEnrollment::okButton_Click);
			// 
			// button2
			// 
			this->button2->DialogResult = System::Windows::Forms::DialogResult::Cancel;
			this->button2->Location = System::Drawing::Point(355, 229);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(90, 24);
			this->button2->TabIndex = 15;
			this->button2->Text = L"Cancel";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &ParticipantEnrollment::button2_Click);
			// 
			// ParticipantEnrollment
			// 
			this->AcceptButton = this->okButton;
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->CancelButton = this->button2;
			this->ClientSize = System::Drawing::Size(457, 261);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->okButton);
			this->Controls->Add(this->groupBox1);
			this->MaximizeBox = false;
			this->Name = L"ParticipantEnrollment";
			this->ShowIcon = false;
			this->Text = L"New Participant";
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->ResumeLayout(false);

		}
#pragma endregion
private: System::Collections::IEnumerator^ partenum;
private: TextBox^ optName,^optAddr;
private: DateTimePicker^ optDate;
public: String^ parentID;
public: void Update(System::Collections::IEnumerator^ my_enum, String^ type, Windows::Forms::DateTimePicker ^ dob,TextBox^ optNameText, TextBox^ optAddrText){
			//String^ tmp;
			update = true;
			this->Text = "Edit Participant";
			this->dobField->Value = dob->Value;
			this->okButton->Text = "Update";
			this->typeCombo->Text = type;
			this->typeCombo->Enabled = false;
			this->nameText->Enabled = true;
			this->dobField->Enabled = true;
			this->addressText->Enabled = true;
			this->raceCombo->Enabled = true;
			this->disabilitiesText->Enabled = true;
			partenum = my_enum;
			optName = optNameText;
			optAddr = optAddrText;
			optDate = dob;
			my_enum->MoveNext();
			DataGridViewTextBoxCell^ my_cell = (cli::safe_cast<DataGridViewTextBoxCell ^>(my_enum->Current));
			id = my_cell->Value->ToString();
			my_enum->MoveNext();
			my_cell = (cli::safe_cast<DataGridViewTextBoxCell ^>(my_enum->Current));
			this->nameText->Text = (my_cell->Value)->ToString();
			my_enum->MoveNext();
			my_cell = (cli::safe_cast<DataGridViewTextBoxCell ^>(my_enum->Current));
			this->addressText->Text = (my_cell->Value)->ToString();
			my_enum->MoveNext();
			//DataGridViewCell^ my_cell2 = (cli::safe_cast<DataGridViewCell ^>(my_enum->Current));
			//this->dobField->Text = (my_cell->Value);
			my_enum->MoveNext();
			my_cell = (cli::safe_cast<DataGridViewTextBoxCell ^>(my_enum->Current));
			this->raceCombo->Text = (my_cell->Value)->ToString();
			if (type == L"Mother") {
				 this->employmentCombo->Enabled = true;
				 this->maritalCombo->Enabled = true;
				 this->schoolText->Enabled = true;
				 this->languageText->Enabled = true;
				 my_enum->MoveNext();
				 my_cell = (cli::safe_cast<DataGridViewTextBoxCell ^>(my_enum->Current));
				 this->languageText->Text = (my_cell->Value)->ToString();
				 my_enum->MoveNext();
				 my_cell = (cli::safe_cast<DataGridViewTextBoxCell ^>(my_enum->Current));
				 this->employmentCombo->Text = (my_cell->Value)->ToString();
				 my_enum->MoveNext();
				 my_cell = (cli::safe_cast<DataGridViewTextBoxCell ^>(my_enum->Current));
				 this->maritalCombo->Text = (my_cell->Value)->ToString();
				 my_enum->MoveNext();
				 my_cell = (cli::safe_cast<DataGridViewTextBoxCell ^>(my_enum->Current));
				 this->schoolText->Text = (my_cell->Value)->ToString();
			} else {
				 this->employmentCombo->Enabled = false;
				 this->maritalCombo->Enabled = false;
				 this->schoolText->Enabled = false;
				 this->languageText->Enabled = false;
			}
			my_enum->MoveNext();
			my_cell = (cli::safe_cast<DataGridViewTextBoxCell ^>(my_enum->Current));
			this->disabilitiesText->Text = (my_cell->Value)->ToString();
		 }
public: DBConnection ^dbconn;
public: bool update;
private: String^ id;
private: System::Void typeCombo_SelectionChangeCommitted(System::Object^  sender, System::EventArgs^  e) {
			 //ParticipantEnrollment Entryform code to enable insertion fiels
			 this->nameText->Enabled = true;
			 this->dobField->Enabled = true;
			 this->addressText->Enabled = true;
			 this->raceCombo->Enabled = true;
			 this->disabilitiesText->Enabled = true;
			 if (this->typeCombo->SelectedIndex == 0) {
				 this->employmentCombo->Enabled = true;
				 this->maritalCombo->Enabled = true;
				 this->schoolText->Enabled = true;
				 this->languageText->Enabled = true;
			 } else {
				 this->employmentCombo->Enabled = false;
				 this->maritalCombo->Enabled = false;
				 this->schoolText->Enabled = false;
				 this->languageText->Enabled = false;
			 }
		 }
private: System::Void okButton_Click(System::Object^  sender, System::EventArgs^  e) {
			 //dbcon->Save(nameText->Text);
			 //Insert(String ^table,String ^tuple)
			 DataGridViewTextBoxCell^ my_cell;
			 String ^ tableName = this->typeCombo->Text;
			 if(tableName == "Mother") tableName = "Adult";
			 String ^query = "UPDATE " + tableName + " SET ";
			 //.ToString();
			 //dbconn->Insert(this->typeCombo->Text, String::Format("(0),(1),(2),(3),(4),(5),(6),(7),(8),(9),(10)","",this->nameText->Text,this->addressText->Text,this->dobText->Text,this->raceCombo->Text,this->languageText->Text,this->employmentCombo->Text,this->maritalCombo->Text,this->schoolText->Text,this->disabilitiesText->Text,""));
			 if(update) {
				 if(this->typeCombo->Text == "Mother" || this->typeCombo->Text == "Adult") {
					 //dbconn->RunNonQuery(String::Format());
					 partenum->Reset();
					 partenum->MoveNext();
					 partenum->MoveNext();
					 my_cell = (cli::safe_cast<DataGridViewTextBoxCell ^>(partenum->Current));
					 if(my_cell->Value->ToString() != this->nameText->Text)
					 {
						 my_cell->Value = (cli::safe_cast<System::Object ^ >(this->nameText->Text));
						 optName->Text = this->nameText->Text;
						 query += "Name = '" + nameText->Text + "', ";
					 }
					 partenum->MoveNext();
					 my_cell = (cli::safe_cast<DataGridViewTextBoxCell ^>(partenum->Current));
					if(my_cell->Value->ToString() != this->addressText->Text)
					{
						my_cell->Value = (cli::safe_cast<System::Object ^ >(this->addressText->Text));
						optAddr->Text = this->addressText->Text;
						query += "Address = '" + addressText->Text + "', ";
					}
					partenum->MoveNext();
					 my_cell = (cli::safe_cast<DataGridViewTextBoxCell ^>(partenum->Current));
					 if(optDate->Value != this->dobField->Value) {
						optDate->Value = this->dobField->Value;
						String  ^dob = this->dobField->Text;
						String ^year = dob->Substring(0,4);
						String ^month = dob->Substring(5,2);
						String ^day = dob->Substring(8,2);
						my_cell->Value = (cli::safe_cast<System::Object ^ >(
							String::Format("{0}/{1}/{2}",month,day,year)));
						query += "DateOfBirth = '" + dobField->Text + "', ";
					 }
					 partenum->MoveNext();
					 my_cell = (cli::safe_cast<DataGridViewTextBoxCell ^>(partenum->Current));
					if(my_cell->Value->ToString() != this->raceCombo->Text)
					{
						my_cell->Value = (cli::safe_cast<System::Object ^ >(this->raceCombo->Text));
						query += "Race = " + (raceCombo->SelectedIndex + 1) + ", ";
					}
					partenum->MoveNext();
					my_cell = (cli::safe_cast<DataGridViewTextBoxCell ^>(partenum->Current));
					if(my_cell->Value->ToString() != this->languageText->Text)
					{
						my_cell->Value = (cli::safe_cast<System::Object ^ >(this->languageText->Text));
						query += "Languages = '" + languageText->Text + "', ";
					}
					partenum->MoveNext();
					my_cell = (cli::safe_cast<DataGridViewTextBoxCell ^>(partenum->Current));
					if(my_cell->Value->ToString() != this->employmentCombo->Text)
					{
						my_cell->Value = (cli::safe_cast<System::Object ^ >(this->employmentCombo->Text));
						query += "EmploymentStatus = " + (employmentCombo->SelectedIndex + 1) + ", ";
					}
					partenum->MoveNext();
					my_cell = (cli::safe_cast<DataGridViewTextBoxCell ^>(partenum->Current));
					if(my_cell->Value->ToString() != this->maritalCombo->Text)
					{
						my_cell->Value = (cli::safe_cast<System::Object ^ >(this->maritalCombo->Text));
						query += "MaritalStatus = " + (maritalCombo->SelectedIndex + 1) + ", ";
					}
					partenum->MoveNext();
					my_cell = (cli::safe_cast<DataGridViewTextBoxCell ^>(partenum->Current));
					if(my_cell->Value->ToString() != this->schoolText->Text)
					{
						my_cell->Value = (cli::safe_cast<System::Object ^ >(this->schoolText->Text));
						query += "YearsOfSchool = " + schoolText->Text + ", ";
					}
					partenum->MoveNext();
					my_cell = (cli::safe_cast<DataGridViewTextBoxCell ^>(partenum->Current));
					if(my_cell->Value->ToString() != this->disabilitiesText->Text)
					{
						my_cell->Value = (cli::safe_cast<System::Object ^ >(this->disabilitiesText->Text));
						query += "Disabilities = '" + disabilitiesText->Text + "', ";
					}
				 } else {
					 partenum->Reset();
					 partenum->MoveNext();
					 partenum->MoveNext();
					 my_cell = (cli::safe_cast<DataGridViewTextBoxCell ^>(partenum->Current));
					 if(my_cell->Value->ToString() != this->nameText->Text)
					 {
						 my_cell->Value = (cli::safe_cast<System::Object ^ >(this->nameText->Text));
						 optName->Text = this->nameText->Text;
						 query += "Name = '" + nameText->Text + "', ";
					 }
					 partenum->MoveNext();
					 my_cell = (cli::safe_cast<DataGridViewTextBoxCell ^>(partenum->Current));
					if(my_cell->Value->ToString() != this->addressText->Text)
					{
						my_cell->Value = (cli::safe_cast<System::Object ^ >(this->addressText->Text));
						optAddr->Text = this->addressText->Text;
						query += "Address = '" + addressText->Text + "', ";
					}
					partenum->MoveNext();
					 my_cell = (cli::safe_cast<DataGridViewTextBoxCell ^>(partenum->Current));
					 if(optDate->Value != this->dobField->Value) {
						optDate->Value = this->dobField->Value;
						String  ^dob = this->dobField->Text;
						String ^year = dob->Substring(0,4);
						String ^month = dob->Substring(5,2);
						String ^day = dob->Substring(8,2);
						my_cell->Value = (cli::safe_cast<System::Object ^ >(
							String::Format("{0}/{1}/{2}",month,day,year)));
						query += "DateOfBirth = '" + dobField->Text + "', ";
					 }
					 partenum->MoveNext();
					 my_cell = (cli::safe_cast<DataGridViewTextBoxCell ^>(partenum->Current));
					if(my_cell->Value->ToString() != this->raceCombo->Text)
					{
						my_cell->Value = (cli::safe_cast<System::Object ^ >(this->raceCombo->Text));
						query += "Race = " + (raceCombo->SelectedIndex + 1) + ", ";
					}
					partenum->MoveNext();
					my_cell = (cli::safe_cast<DataGridViewTextBoxCell ^>(partenum->Current));
					if(my_cell->Value->ToString() != this->disabilitiesText->Text)
					{
						my_cell->Value = (cli::safe_cast<System::Object ^ >(this->disabilitiesText->Text));
						query += "Disabilities = '" + disabilitiesText->Text + "', ";
					}
				 }

				 //partenum->MoveNext();
				 //my_cell = (cli::safe_cast<DataGridViewTextBoxCell ^>(partenum->Current));
				 //query += "'AssessmentID' = " + my_cell->ToString();
				 query = query->Substring(0,query->Length-2);
				 query += " WHERE ID = " + this->id;
				 if(query != "UPDATE " + tableName + " SE WHERE ID = " + this->id)
					dbconn->RunNonQuery(query);
				 //dbconn->RunNonQuery(query);

			 } else {
				 if(this->typeCombo->Text=="Mother")
				 {
					dbconn->Insert("Adult", String::Format(
						"({0},'{1}','{2}','{3}',{4},'{5}',{6},{7},'{8}','{9}',{10})","NULL",
						this->nameText->Text,this->addressText->Text,this->dobField->Text,
						(this->raceCombo->SelectedIndex + 1),this->languageText->Text,
						(this->employmentCombo->SelectedIndex + 1),(this->maritalCombo->SelectedIndex + 1),
						this->schoolText->Text,this->disabilitiesText->Text,"NULL"));
				 }
				 else if(this->typeCombo->Text=="Child")
				 {
					dbconn->Insert("Child", String::Format(
						"({0},'{1}','{2}','{3}',{4},'{5}',{6},{7})","NULL",this->nameText->Text,
						this->addressText->Text,this->dobField->Text,(this->raceCombo->SelectedIndex + 1),
						this->disabilitiesText->Text,parentID,"NULL"));
				}
				//System::Windows::Forms::MessageBox(*String::Format("(0),(1),(2),(3),(4),(5),(6),(7),(8),(9),(10)","","Eric","65","5/6/1983","White/Caucasian","English","1","1","1","none",""));
				//String::Format("{0},{1},{2},{3}",this->nameText->Text,this->maritalCombo->SelectedItem,this->addressText->Text)
			 }
			 this->Close();
		 }
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
			 this->Close();
		 }
private: System::Void groupBox2_Enter(System::Object^  sender, System::EventArgs^  e) {		 }
private: System::Void ParticipantEnrollment_Load(System::Object^  sender, System::EventArgs^  e) {
		 }
};
}
