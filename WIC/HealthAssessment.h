#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

#include "DBConnection.h"

namespace WIC {

	/// <summary>
	/// Summary for HealthAssessment
	///
	/// WARNING: If you change the name of this class, you will need to change the
	///          'Resource File Name' property for the managed resource compiler tool
	///          associated with all .resx files this class depends on.  Otherwise,
	///          the designers will not be able to interact properly with localized
	///          resources associated with this form.
	/// </summary>
	public ref class HealthAssessment : public System::Windows::Forms::Form
	{
	public:
		HealthAssessment(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~HealthAssessment()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::GroupBox^  groupBox1;
	private: System::Windows::Forms::GroupBox^  groupBox2;
	private: System::Windows::Forms::DateTimePicker^  dobField;


	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::TextBox^  nameText;

	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::TextBox^  alcoholText;


	private: System::Windows::Forms::Label^  label8;
	private: System::Windows::Forms::TextBox^  hemoglobinText;

	private: System::Windows::Forms::Label^  label7;
	private: System::Windows::Forms::TextBox^  weightText;

	private: System::Windows::Forms::TextBox^  heightText;

	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::TextBox^  vitaminsText;

	private: System::Windows::Forms::Label^  label10;
	private: System::Windows::Forms::ComboBox^  smokerCombo;

	private: System::Windows::Forms::Label^  label9;
	private: System::Windows::Forms::Button^  cancelButton;
	public: System::Windows::Forms::Button^  okButton;
	private: 

	private: System::Windows::Forms::TextBox^  typeText;

	private: System::Windows::Forms::Label^  label11;
	protected: 

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->typeText = (gcnew System::Windows::Forms::TextBox());
			this->label11 = (gcnew System::Windows::Forms::Label());
			this->dobField = (gcnew System::Windows::Forms::DateTimePicker());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->nameText = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->cancelButton = (gcnew System::Windows::Forms::Button());
			this->okButton = (gcnew System::Windows::Forms::Button());
			this->vitaminsText = (gcnew System::Windows::Forms::TextBox());
			this->label10 = (gcnew System::Windows::Forms::Label());
			this->smokerCombo = (gcnew System::Windows::Forms::ComboBox());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->alcoholText = (gcnew System::Windows::Forms::TextBox());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->hemoglobinText = (gcnew System::Windows::Forms::TextBox());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->weightText = (gcnew System::Windows::Forms::TextBox());
			this->heightText = (gcnew System::Windows::Forms::TextBox());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->groupBox1->SuspendLayout();
			this->groupBox2->SuspendLayout();
			this->SuspendLayout();
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->typeText);
			this->groupBox1->Controls->Add(this->label11);
			this->groupBox1->Controls->Add(this->dobField);
			this->groupBox1->Controls->Add(this->label2);
			this->groupBox1->Controls->Add(this->nameText);
			this->groupBox1->Controls->Add(this->label1);
			this->groupBox1->Location = System::Drawing::Point(12, 12);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(410, 119);
			this->groupBox1->TabIndex = 0;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"Participant Info";
			// 
			// typeText
			// 
			this->typeText->Location = System::Drawing::Point(275, 17);
			this->typeText->Name = L"typeText";
			this->typeText->ReadOnly = true;
			this->typeText->Size = System::Drawing::Size(100, 20);
			this->typeText->TabIndex = 2;
			// 
			// label11
			// 
			this->label11->AutoSize = true;
			this->label11->Location = System::Drawing::Point(235, 20);
			this->label11->Name = L"label11";
			this->label11->Size = System::Drawing::Size(34, 13);
			this->label11->TabIndex = 4;
			this->label11->Text = L"Type:";
			// 
			// dobField
			// 
			this->dobField->CustomFormat = L"yyyy-MM-dd";
			this->dobField->Enabled = false;
			this->dobField->Format = System::Windows::Forms::DateTimePickerFormat::Custom;
			this->dobField->Location = System::Drawing::Point(79, 43);
			this->dobField->Name = L"dobField";
			this->dobField->Size = System::Drawing::Size(89, 20);
			this->dobField->TabIndex = 3;
			this->dobField->Value = System::DateTime(2000, 1, 1, 22, 47, 0, 0);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(5, 47);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(68, 13);
			this->label2->TabIndex = 2;
			this->label2->Text = L"Date of birth:";
			// 
			// nameText
			// 
			this->nameText->Location = System::Drawing::Point(51, 17);
			this->nameText->Name = L"nameText";
			this->nameText->ReadOnly = true;
			this->nameText->Size = System::Drawing::Size(162, 20);
			this->nameText->TabIndex = 1;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(7, 20);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(38, 13);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Name:";
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->cancelButton);
			this->groupBox2->Controls->Add(this->okButton);
			this->groupBox2->Controls->Add(this->vitaminsText);
			this->groupBox2->Controls->Add(this->label10);
			this->groupBox2->Controls->Add(this->smokerCombo);
			this->groupBox2->Controls->Add(this->label9);
			this->groupBox2->Controls->Add(this->alcoholText);
			this->groupBox2->Controls->Add(this->label8);
			this->groupBox2->Controls->Add(this->hemoglobinText);
			this->groupBox2->Controls->Add(this->label7);
			this->groupBox2->Controls->Add(this->weightText);
			this->groupBox2->Controls->Add(this->heightText);
			this->groupBox2->Controls->Add(this->label6);
			this->groupBox2->Controls->Add(this->label5);
			this->groupBox2->Controls->Add(this->label4);
			this->groupBox2->Controls->Add(this->label3);
			this->groupBox2->Location = System::Drawing::Point(12, 137);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Size = System::Drawing::Size(409, 166);
			this->groupBox2->TabIndex = 1;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"Assessment Info";
			// 
			// cancelButton
			// 
			this->cancelButton->DialogResult = System::Windows::Forms::DialogResult::Cancel;
			this->cancelButton->Location = System::Drawing::Point(264, 106);
			this->cancelButton->Name = L"cancelButton";
			this->cancelButton->Size = System::Drawing::Size(126, 45);
			this->cancelButton->TabIndex = 11;
			this->cancelButton->Text = L"Cancel";
			this->cancelButton->UseVisualStyleBackColor = true;
			this->cancelButton->Click += gcnew System::EventHandler(this, &HealthAssessment::cancelButton_Click);
			// 
			// okButton
			// 
			this->okButton->Location = System::Drawing::Point(30, 106);
			this->okButton->Name = L"okButton";
			this->okButton->Size = System::Drawing::Size(126, 45);
			this->okButton->TabIndex = 10;
			this->okButton->Text = L"OK";
			this->okButton->UseVisualStyleBackColor = true;
			this->okButton->Click += gcnew System::EventHandler(this, &HealthAssessment::okButton_Click);
			// 
			// vitaminsText
			// 
			this->vitaminsText->Location = System::Drawing::Point(290, 70);
			this->vitaminsText->Name = L"vitaminsText";
			this->vitaminsText->Size = System::Drawing::Size(101, 20);
			this->vitaminsText->TabIndex = 9;
			// 
			// label10
			// 
			this->label10->AutoSize = true;
			this->label10->Location = System::Drawing::Point(235, 73);
			this->label10->Name = L"label10";
			this->label10->Size = System::Drawing::Size(49, 13);
			this->label10->TabIndex = 12;
			this->label10->Text = L"Vitamins:";
			// 
			// smokerCombo
			// 
			this->smokerCombo->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->smokerCombo->FormattingEnabled = true;
			this->smokerCombo->Items->AddRange(gcnew cli::array< System::Object^  >(2) {L"No", L"Yes"});
			this->smokerCombo->Location = System::Drawing::Point(58, 69);
			this->smokerCombo->Name = L"smokerCombo";
			this->smokerCombo->Size = System::Drawing::Size(65, 21);
			this->smokerCombo->TabIndex = 8;
			// 
			// label9
			// 
			this->label9->AutoSize = true;
			this->label9->Location = System::Drawing::Point(6, 72);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(46, 13);
			this->label9->TabIndex = 10;
			this->label9->Text = L"Smoker:";
			// 
			// alcoholText
			// 
			this->alcoholText->Location = System::Drawing::Point(291, 43);
			this->alcoholText->Name = L"alcoholText";
			this->alcoholText->Size = System::Drawing::Size(100, 20);
			this->alcoholText->TabIndex = 7;
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Location = System::Drawing::Point(206, 46);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(79, 13);
			this->label8->TabIndex = 8;
			this->label8->Text = L"Alcohol Usage:";
			// 
			// hemoglobinText
			// 
			this->hemoglobinText->Location = System::Drawing::Point(78, 43);
			this->hemoglobinText->Name = L"hemoglobinText";
			this->hemoglobinText->Size = System::Drawing::Size(45, 20);
			this->hemoglobinText->TabIndex = 6;
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(6, 46);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(66, 13);
			this->label7->TabIndex = 6;
			this->label7->Text = L"Hemoglobin:";
			// 
			// weightText
			// 
			this->weightText->Location = System::Drawing::Point(291, 17);
			this->weightText->Name = L"weightText";
			this->weightText->Size = System::Drawing::Size(53, 20);
			this->weightText->TabIndex = 5;
			// 
			// heightText
			// 
			this->heightText->Location = System::Drawing::Point(53, 17);
			this->heightText->Name = L"heightText";
			this->heightText->Size = System::Drawing::Size(70, 20);
			this->heightText->TabIndex = 4;
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(241, 20);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(44, 13);
			this->label6->TabIndex = 3;
			this->label6->Text = L"Weight:";
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(350, 20);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(20, 13);
			this->label5->TabIndex = 2;
			this->label5->Text = L"lbs";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(130, 20);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(38, 13);
			this->label4->TabIndex = 1;
			this->label4->Text = L"inches";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(6, 20);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(41, 13);
			this->label3->TabIndex = 0;
			this->label3->Text = L"Height:";
			// 
			// HealthAssessment
			// 
			this->AcceptButton = this->okButton;
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->CancelButton = this->cancelButton;
			this->ClientSize = System::Drawing::Size(434, 315);
			this->Controls->Add(this->groupBox2);
			this->Controls->Add(this->groupBox1);
			this->MaximizeBox = false;
			this->Name = L"HealthAssessment";
			this->ShowIcon = false;
			this->Text = L"Health Assessment";
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->groupBox2->ResumeLayout(false);
			this->groupBox2->PerformLayout();
			this->ResumeLayout(false);

		}
#pragma endregion
public: bool update;
public: String^ userID;
private: static int hid = 0;
public: DBConnection ^dbconn;
private: DataGridViewTextBoxCell ^ assIDCell;
private: cli::array<String ^> ^values;
public: System::Void Update(String^ id, String^ name, String^ type, Windows::Forms::DateTimePicker ^ dob, DataGridViewTextBoxCell ^ cell)
		{
			this->nameText->Text = name;
			this->userID = id;
			this->typeText->Text = type;
			this->dobField->Value = dob->Value;
			this->assIDCell = cell;
			if(update) {
				this->okButton->Text = "Update";
				values = dbconn->Get("HealthAssessment","AssessmentID",assIDCell->Value->ToString(),7);
				this->userID = values[0];
				this->heightText->Text = values[1];
				this->weightText->Text = values[2];
				this->hemoglobinText->Text = values[3];
				this->alcoholText->Text = values[4];
				this->smokerCombo->Text = values[5];
				this->vitaminsText->Text = values[6];
			}
		}
private: System::Void okButton_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (update)
			 {
				 String ^ query = "UPDATE HealthAssessment SET ";
				 if(values[1] != this->heightText->Text) {
					 query += "Height = " + heightText->Text + ", ";
				 }
				 if(values[2] != this->weightText->Text) {
					 query += "Weight = " + weightText->Text + ", ";
				 }
				 if(values[3] != this->hemoglobinText->Text) {
					 query += "Hemoglobin = " + hemoglobinText->Text + ", ";
				 }
				 if(values[4] != this->alcoholText->Text) {
					 query += "AlcoholUsage = '" + alcoholText->Text + "', ";
				 }
				 if(values[5] != this->smokerCombo->Text) {
					 query += "Smoker = " + (smokerCombo->SelectedIndex + 1) + ", ";
				 }
				 if(values[6] != this->vitaminsText->Text) {
					 query += "Vitamins = '" + vitaminsText->Text + "', ";
				 }
				 query = query->Substring(0, query->Length - 2);
				 query += (" WHERE AssessmentID = " + values[0]);
				 if(query != "UPDATE HealthAssessment SE WHERE AssessmentID = " + values[0])
					dbconn->RunNonQuery(query);
				 this->Close();
			 }
			 else
			 {
				 if(!hid) {
					String^ tempID = dbconn->GetID("HealthAssessment","AssessmentID");
					hid = Int32::Parse(tempID) + 1;
				 }
				 dbconn->Insert("HealthAssessment", String::Format(
					"({0},'{1}','{2}','{3}','{4}','{5}','{6}')", hid, this->heightText->Text, 
					this->weightText->Text, this->hemoglobinText->Text, this->alcoholText->Text, 
					(this->smokerCombo->SelectedIndex+1),this->vitaminsText->Text));
				 assIDCell->Value = (cli::safe_cast<System::Object ^ >(hid));
				 //dbconn->Update();
				 String^ tableName = this->typeText->Text;
				 if (tableName == "Mother") tableName = "Adult";
				 dbconn->RunNonQuery("UPDATE " + tableName + " SET AssessmentID = " + hid + " WHERE ID = " + userID);
				 hid++;
				 this->Close();
			 }
		 }
private: System::Void cancelButton_Click(System::Object^  sender, System::EventArgs^  e) {
			 this->Close();
		 }
};
}
