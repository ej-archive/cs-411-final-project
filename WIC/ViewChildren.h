#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

#include "DBConnection.h"
#include "ParticipantEnrollment.h"

namespace WIC {

	/// <summary>
	/// Summary for ViewChildren
	///
	/// WARNING: If you change the name of this class, you will need to change the
	///          'Resource File Name' property for the managed resource compiler tool
	///          associated with all .resx files this class depends on.  Otherwise,
	///          the designers will not be able to interact properly with localized
	///          resources associated with this form.
	/// </summary>
	public ref class ViewChildren : public System::Windows::Forms::Form
	{
	public:
		ViewChildren(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~ViewChildren()
		{
			if (components)
			{
				delete components;
			}
		}
	public: System::Windows::Forms::DataGridView^  searchView;
	protected: 

	protected: 

	protected: 
	private: System::Windows::Forms::Button^  addButton;

	protected: 









	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->searchView = (gcnew System::Windows::Forms::DataGridView());
			this->addButton = (gcnew System::Windows::Forms::Button());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->searchView))->BeginInit();
			this->SuspendLayout();
			// 
			// searchView
			// 
			this->searchView->AllowUserToAddRows = false;
			this->searchView->AllowUserToOrderColumns = true;
			this->searchView->AllowUserToResizeRows = false;
			this->searchView->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
				| System::Windows::Forms::AnchorStyles::Left) 
				| System::Windows::Forms::AnchorStyles::Right));
			this->searchView->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::AllCells;
			this->searchView->AutoSizeRowsMode = System::Windows::Forms::DataGridViewAutoSizeRowsMode::AllCells;
			this->searchView->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->searchView->Location = System::Drawing::Point(12, 39);
			this->searchView->Name = L"searchView";
			this->searchView->ReadOnly = true;
			this->searchView->SelectionMode = System::Windows::Forms::DataGridViewSelectionMode::FullRowSelect;
			this->searchView->Size = System::Drawing::Size(422, 155);
			this->searchView->TabIndex = 14;
			this->searchView->UserDeletingRow += gcnew System::Windows::Forms::DataGridViewRowCancelEventHandler(this, &ViewChildren::searchView_UserDeletingRow);
			this->searchView->CellDoubleClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &ViewChildren::searchView_CellDoubleClick);
			this->searchView->UserDeletedRow += gcnew System::Windows::Forms::DataGridViewRowEventHandler(this, &ViewChildren::searchView_UserDeletedRow);
			this->searchView->RowHeaderMouseDoubleClick += gcnew System::Windows::Forms::DataGridViewCellMouseEventHandler(this, &ViewChildren::searchView_RowHeaderMouseDoubleClick);
			// 
			// addButton
			// 
			this->addButton->Location = System::Drawing::Point(12, 12);
			this->addButton->Name = L"addButton";
			this->addButton->Size = System::Drawing::Size(116, 21);
			this->addButton->TabIndex = 12;
			this->addButton->Text = L"Add Child";
			this->addButton->UseVisualStyleBackColor = true;
			this->addButton->Click += gcnew System::EventHandler(this, &ViewChildren::addButton_Click);
			// 
			// ViewChildren
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(446, 206);
			this->Controls->Add(this->addButton);
			this->Controls->Add(this->searchView);
			this->Name = L"ViewChildren";
			this->Text = L"ViewChildren";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->searchView))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
public: DBConnection^ dbconn;
private: ParticipantEnrollment^ partForm;
public: String^ parentID;
private: System::Void searchView_UserDeletingRow(System::Object^  sender, System::Windows::Forms::DataGridViewRowCancelEventArgs^  e) {
			 System::Windows::Forms::DialogResult result;
			 result = MessageBox::Show("Are you sure you would like to delete this row?","Delete?",
			 MessageBoxButtons::YesNo,MessageBoxIcon::Question,MessageBoxDefaultButton::Button1);
			 if(result == System::Windows::Forms::DialogResult::No)
				 e->Cancel = true;
			 }
private: System::Void searchView_UserDeletedRow(System::Object^  sender, System::Windows::Forms::DataGridViewRowEventArgs^  e) {
				 dbconn->Update();
			 }
private: System::Void searchView_RowHeaderMouseDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e) {
			 
		 }
private: System::Void searchView_CellDoubleClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e) {
			 this->searchView_RowHeaderMouseDoubleClick(sender,nullptr);
		 }
private: System::Void optionsWindowClosed(System::Object^  sender, System::Windows::Forms::FormClosedEventArgs^  e) {
			 //partOpts = nullptr;
		 }
private: System::Void addButton_Click(System::Object^  sender, System::EventArgs^  e) {
			this->partForm = (gcnew ParticipantEnrollment());
			this->partForm->MdiParent = this->MdiParent;
			this->partForm->dbconn = this->dbconn;
			this->partForm->update = false;
			this->partForm->typeCombo->SelectedIndex = 1;
			partForm->nameText->Enabled = true;
			partForm->dobField->Enabled = true;
			partForm->addressText->Enabled = true;
			partForm->raceCombo->Enabled = true;
			partForm->disabilitiesText->Enabled = true;
			partForm->FormClosed += gcnew System::Windows::Forms::FormClosedEventHandler(this, &ViewChildren::newChildWindowClosed); 
			this->partForm->typeCombo->Enabled = false;
			this->partForm->parentID = parentID;
			this->partForm->Show();
		 }
private: System::Void newChildWindowClosed(System::Object^  sender, System::Windows::Forms::FormClosedEventArgs^  e) {
			 dbconn->Search(this->searchView, "Child","ParentID = " + parentID, "*");
		 }
};
}
