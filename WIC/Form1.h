#pragma once
#include "ServiceEntry.h"
#include "ParticipantEnrollment.h"
#include "HealthAssessment.h"
#include "PartSearch.h"
#include "ConnectForm.h"
#include "DBConnection.h"

namespace WIC {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Form1
	///
	/// WARNING: If you change the name of this class, you will need to change the
	///          'Resource File Name' property for the managed resource compiler tool
	///          associated with all .resx files this class depends on.  Otherwise,
	///          the designers will not be able to interact properly with localized
	///          resources associated with this form.
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
			if (childForm)
				delete childForm;
		}
	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	protected: 








	private: System::Windows::Forms::ToolStrip^  toolStrip1;








	private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator;








	private: System::Windows::Forms::ToolStripMenuItem^  fileToolStripMenuItem1;
	private: System::Windows::Forms::ToolStripMenuItem^  newToolStripMenuItem1;



	private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator2;


	private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator3;
	private: System::Windows::Forms::ToolStripMenuItem^  printToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  printPreviewToolStripMenuItem;
	private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator4;
	private: System::Windows::Forms::ToolStripMenuItem^  exitToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  editToolStripMenuItem1;
	private: System::Windows::Forms::ToolStripMenuItem^  undoToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  redoToolStripMenuItem;
	private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator5;
	private: System::Windows::Forms::ToolStripMenuItem^  cutToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  copyToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  pasteToolStripMenuItem;
	private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator6;
	private: System::Windows::Forms::ToolStripMenuItem^  selectAllToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  toolsToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  customizeToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  optionsToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  helpToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  contentsToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  indexToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  searchToolStripMenuItem1;
	private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator7;
	private: System::Windows::Forms::ToolStripMenuItem^  aboutToolStripMenuItem;

	private: ServiceEntry^	childForm;
	private: ParticipantEnrollment^	partForm;


	private: System::Windows::Forms::ToolStripButton^  connButton;

	private: System::Windows::Forms::ToolStripButton^  disconnButton;
	private: System::Windows::Forms::ToolStripButton^  searchButton;
	private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem1;
	private: System::Windows::Forms::ToolStripMenuItem^  disconnectToolStripMenuItem;
	private: System::Windows::Forms::ToolStripButton^  newPartButton;




	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Form1::typeid));
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->fileToolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->newToolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->disconnectToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripSeparator2 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->toolStripSeparator3 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->printToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->printPreviewToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripSeparator4 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->exitToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->editToolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->undoToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->redoToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripSeparator5 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->cutToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->copyToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->pasteToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripSeparator6 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->selectAllToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->customizeToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->optionsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->helpToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->contentsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->indexToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->searchToolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripSeparator7 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->aboutToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStrip1 = (gcnew System::Windows::Forms::ToolStrip());
			this->searchButton = (gcnew System::Windows::Forms::ToolStripButton());
			this->toolStripSeparator = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->connButton = (gcnew System::Windows::Forms::ToolStripButton());
			this->disconnButton = (gcnew System::Windows::Forms::ToolStripButton());
			this->newPartButton = (gcnew System::Windows::Forms::ToolStripButton());
			this->menuStrip1->SuspendLayout();
			this->toolStrip1->SuspendLayout();
			this->SuspendLayout();
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(4) {this->fileToolStripMenuItem1, 
				this->editToolStripMenuItem1, this->toolsToolStripMenuItem, this->helpToolStripMenuItem});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(710, 24);
			this->menuStrip1->TabIndex = 0;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// fileToolStripMenuItem1
			// 
			this->fileToolStripMenuItem1->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(9) {this->newToolStripMenuItem1, 
				this->toolStripMenuItem1, this->disconnectToolStripMenuItem, this->toolStripSeparator2, this->toolStripSeparator3, this->printToolStripMenuItem, 
				this->printPreviewToolStripMenuItem, this->toolStripSeparator4, this->exitToolStripMenuItem});
			this->fileToolStripMenuItem1->Name = L"fileToolStripMenuItem1";
			this->fileToolStripMenuItem1->Size = System::Drawing::Size(35, 20);
			this->fileToolStripMenuItem1->Text = L"&File";
			// 
			// newToolStripMenuItem1
			// 
			this->newToolStripMenuItem1->BackgroundImageLayout = System::Windows::Forms::ImageLayout::None;
			this->newToolStripMenuItem1->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"newToolStripMenuItem1.Image")));
			this->newToolStripMenuItem1->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->newToolStripMenuItem1->Name = L"newToolStripMenuItem1";
			this->newToolStripMenuItem1->ShowShortcutKeys = false;
			this->newToolStripMenuItem1->Size = System::Drawing::Size(165, 22);
			this->newToolStripMenuItem1->Text = L"&New Participant";
			this->newToolStripMenuItem1->Click += gcnew System::EventHandler(this, &Form1::programParticipantToolStripMenuItem1_Click);
			// 
			// toolStripMenuItem1
			// 
			this->toolStripMenuItem1->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"toolStripMenuItem1.Image")));
			this->toolStripMenuItem1->Name = L"toolStripMenuItem1";
			this->toolStripMenuItem1->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::C));
			this->toolStripMenuItem1->Size = System::Drawing::Size(165, 22);
			this->toolStripMenuItem1->Text = L"Connect";
			this->toolStripMenuItem1->Click += gcnew System::EventHandler(this, &Form1::toolStripButton1_Click);
			// 
			// disconnectToolStripMenuItem
			// 
			this->disconnectToolStripMenuItem->Enabled = false;
			this->disconnectToolStripMenuItem->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"disconnectToolStripMenuItem.Image")));
			this->disconnectToolStripMenuItem->Name = L"disconnectToolStripMenuItem";
			this->disconnectToolStripMenuItem->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::D));
			this->disconnectToolStripMenuItem->Size = System::Drawing::Size(165, 22);
			this->disconnectToolStripMenuItem->Text = L"Disconnect";
			this->disconnectToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::disconnButton_Click);
			// 
			// toolStripSeparator2
			// 
			this->toolStripSeparator2->Name = L"toolStripSeparator2";
			this->toolStripSeparator2->Size = System::Drawing::Size(162, 6);
			// 
			// toolStripSeparator3
			// 
			this->toolStripSeparator3->Name = L"toolStripSeparator3";
			this->toolStripSeparator3->Size = System::Drawing::Size(162, 6);
			// 
			// printToolStripMenuItem
			// 
			this->printToolStripMenuItem->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"printToolStripMenuItem.Image")));
			this->printToolStripMenuItem->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->printToolStripMenuItem->Name = L"printToolStripMenuItem";
			this->printToolStripMenuItem->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::P));
			this->printToolStripMenuItem->Size = System::Drawing::Size(165, 22);
			this->printToolStripMenuItem->Text = L"&Print";
			// 
			// printPreviewToolStripMenuItem
			// 
			this->printPreviewToolStripMenuItem->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"printPreviewToolStripMenuItem.Image")));
			this->printPreviewToolStripMenuItem->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->printPreviewToolStripMenuItem->Name = L"printPreviewToolStripMenuItem";
			this->printPreviewToolStripMenuItem->Size = System::Drawing::Size(165, 22);
			this->printPreviewToolStripMenuItem->Text = L"Print Pre&view";
			// 
			// toolStripSeparator4
			// 
			this->toolStripSeparator4->Name = L"toolStripSeparator4";
			this->toolStripSeparator4->Size = System::Drawing::Size(162, 6);
			// 
			// exitToolStripMenuItem
			// 
			this->exitToolStripMenuItem->Name = L"exitToolStripMenuItem";
			this->exitToolStripMenuItem->Size = System::Drawing::Size(165, 22);
			this->exitToolStripMenuItem->Text = L"E&xit";
			// 
			// editToolStripMenuItem1
			// 
			this->editToolStripMenuItem1->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(8) {this->undoToolStripMenuItem, 
				this->redoToolStripMenuItem, this->toolStripSeparator5, this->cutToolStripMenuItem, this->copyToolStripMenuItem, this->pasteToolStripMenuItem, 
				this->toolStripSeparator6, this->selectAllToolStripMenuItem});
			this->editToolStripMenuItem1->Name = L"editToolStripMenuItem1";
			this->editToolStripMenuItem1->Size = System::Drawing::Size(37, 20);
			this->editToolStripMenuItem1->Text = L"&Edit";
			// 
			// undoToolStripMenuItem
			// 
			this->undoToolStripMenuItem->Name = L"undoToolStripMenuItem";
			this->undoToolStripMenuItem->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::Z));
			this->undoToolStripMenuItem->Size = System::Drawing::Size(152, 22);
			this->undoToolStripMenuItem->Text = L"&Undo";
			// 
			// redoToolStripMenuItem
			// 
			this->redoToolStripMenuItem->Name = L"redoToolStripMenuItem";
			this->redoToolStripMenuItem->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::Y));
			this->redoToolStripMenuItem->Size = System::Drawing::Size(152, 22);
			this->redoToolStripMenuItem->Text = L"&Redo";
			// 
			// toolStripSeparator5
			// 
			this->toolStripSeparator5->Name = L"toolStripSeparator5";
			this->toolStripSeparator5->Size = System::Drawing::Size(149, 6);
			// 
			// cutToolStripMenuItem
			// 
			this->cutToolStripMenuItem->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"cutToolStripMenuItem.Image")));
			this->cutToolStripMenuItem->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->cutToolStripMenuItem->Name = L"cutToolStripMenuItem";
			this->cutToolStripMenuItem->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::X));
			this->cutToolStripMenuItem->Size = System::Drawing::Size(152, 22);
			this->cutToolStripMenuItem->Text = L"Cu&t";
			// 
			// copyToolStripMenuItem
			// 
			this->copyToolStripMenuItem->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"copyToolStripMenuItem.Image")));
			this->copyToolStripMenuItem->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->copyToolStripMenuItem->Name = L"copyToolStripMenuItem";
			this->copyToolStripMenuItem->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::C));
			this->copyToolStripMenuItem->Size = System::Drawing::Size(152, 22);
			this->copyToolStripMenuItem->Text = L"&Copy";
			// 
			// pasteToolStripMenuItem
			// 
			this->pasteToolStripMenuItem->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"pasteToolStripMenuItem.Image")));
			this->pasteToolStripMenuItem->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->pasteToolStripMenuItem->Name = L"pasteToolStripMenuItem";
			this->pasteToolStripMenuItem->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::V));
			this->pasteToolStripMenuItem->Size = System::Drawing::Size(152, 22);
			this->pasteToolStripMenuItem->Text = L"&Paste";
			// 
			// toolStripSeparator6
			// 
			this->toolStripSeparator6->Name = L"toolStripSeparator6";
			this->toolStripSeparator6->Size = System::Drawing::Size(149, 6);
			// 
			// selectAllToolStripMenuItem
			// 
			this->selectAllToolStripMenuItem->Name = L"selectAllToolStripMenuItem";
			this->selectAllToolStripMenuItem->Size = System::Drawing::Size(152, 22);
			this->selectAllToolStripMenuItem->Text = L"Select &All";
			// 
			// toolsToolStripMenuItem
			// 
			this->toolsToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->customizeToolStripMenuItem, 
				this->optionsToolStripMenuItem});
			this->toolsToolStripMenuItem->Name = L"toolsToolStripMenuItem";
			this->toolsToolStripMenuItem->Size = System::Drawing::Size(44, 20);
			this->toolsToolStripMenuItem->Text = L"&Tools";
			// 
			// customizeToolStripMenuItem
			// 
			this->customizeToolStripMenuItem->Name = L"customizeToolStripMenuItem";
			this->customizeToolStripMenuItem->Size = System::Drawing::Size(123, 22);
			this->customizeToolStripMenuItem->Text = L"&Customize";
			// 
			// optionsToolStripMenuItem
			// 
			this->optionsToolStripMenuItem->Name = L"optionsToolStripMenuItem";
			this->optionsToolStripMenuItem->Size = System::Drawing::Size(123, 22);
			this->optionsToolStripMenuItem->Text = L"&Options";
			// 
			// helpToolStripMenuItem
			// 
			this->helpToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(5) {this->contentsToolStripMenuItem, 
				this->indexToolStripMenuItem, this->searchToolStripMenuItem1, this->toolStripSeparator7, this->aboutToolStripMenuItem});
			this->helpToolStripMenuItem->Name = L"helpToolStripMenuItem";
			this->helpToolStripMenuItem->Size = System::Drawing::Size(40, 20);
			this->helpToolStripMenuItem->Text = L"&Help";
			// 
			// contentsToolStripMenuItem
			// 
			this->contentsToolStripMenuItem->Name = L"contentsToolStripMenuItem";
			this->contentsToolStripMenuItem->Size = System::Drawing::Size(118, 22);
			this->contentsToolStripMenuItem->Text = L"&Contents";
			// 
			// indexToolStripMenuItem
			// 
			this->indexToolStripMenuItem->Name = L"indexToolStripMenuItem";
			this->indexToolStripMenuItem->Size = System::Drawing::Size(118, 22);
			this->indexToolStripMenuItem->Text = L"&Index";
			// 
			// searchToolStripMenuItem1
			// 
			this->searchToolStripMenuItem1->Name = L"searchToolStripMenuItem1";
			this->searchToolStripMenuItem1->Size = System::Drawing::Size(118, 22);
			this->searchToolStripMenuItem1->Text = L"&Search";
			// 
			// toolStripSeparator7
			// 
			this->toolStripSeparator7->Name = L"toolStripSeparator7";
			this->toolStripSeparator7->Size = System::Drawing::Size(115, 6);
			// 
			// aboutToolStripMenuItem
			// 
			this->aboutToolStripMenuItem->Name = L"aboutToolStripMenuItem";
			this->aboutToolStripMenuItem->Size = System::Drawing::Size(118, 22);
			this->aboutToolStripMenuItem->Text = L"&About...";
			// 
			// toolStrip1
			// 
			this->toolStrip1->GripStyle = System::Windows::Forms::ToolStripGripStyle::Hidden;
			this->toolStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(5) {this->newPartButton, this->searchButton, 
				this->toolStripSeparator, this->connButton, this->disconnButton});
			this->toolStrip1->Location = System::Drawing::Point(0, 24);
			this->toolStrip1->Name = L"toolStrip1";
			this->toolStrip1->Size = System::Drawing::Size(710, 25);
			this->toolStrip1->TabIndex = 1;
			this->toolStrip1->Text = L"toolStrip1";
			// 
			// searchButton
			// 
			this->searchButton->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"searchButton.Image")));
			this->searchButton->ImageTransparentColor = System::Drawing::Color::White;
			this->searchButton->Name = L"searchButton";
			this->searchButton->Size = System::Drawing::Size(60, 22);
			this->searchButton->Text = L"&Search";
			this->searchButton->Click += gcnew System::EventHandler(this, &Form1::searchButton_Click);
			// 
			// toolStripSeparator
			// 
			this->toolStripSeparator->Name = L"toolStripSeparator";
			this->toolStripSeparator->Size = System::Drawing::Size(6, 25);
			// 
			// connButton
			// 
			this->connButton->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"connButton.Image")));
			this->connButton->ImageTransparentColor = System::Drawing::Color::White;
			this->connButton->Name = L"connButton";
			this->connButton->Size = System::Drawing::Size(67, 22);
			this->connButton->Text = L"&Connect";
			this->connButton->Click += gcnew System::EventHandler(this, &Form1::toolStripButton1_Click);
			// 
			// disconnButton
			// 
			this->disconnButton->Enabled = false;
			this->disconnButton->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"disconnButton.Image")));
			this->disconnButton->ImageTransparentColor = System::Drawing::Color::White;
			this->disconnButton->Name = L"disconnButton";
			this->disconnButton->Size = System::Drawing::Size(79, 22);
			this->disconnButton->Text = L"&Disconnect";
			this->disconnButton->Click += gcnew System::EventHandler(this, &Form1::disconnButton_Click);
			// 
			// newPartButton
			// 
			this->newPartButton->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"newPartButton.Image")));
			this->newPartButton->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->newPartButton->Name = L"newPartButton";
			this->newPartButton->Size = System::Drawing::Size(102, 22);
			this->newPartButton->Text = L"&New Participant";
			this->newPartButton->Click += gcnew System::EventHandler(this, &Form1::programParticipantToolStripMenuItem1_Click);
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::SystemColors::AppWorkspace;
			this->ClientSize = System::Drawing::Size(710, 519);
			this->Controls->Add(this->toolStrip1);
			this->Controls->Add(this->menuStrip1);
			this->IsMdiContainer = true;
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"Form1";
			this->Text = L"WIC Program";
			this->WindowState = System::Windows::Forms::FormWindowState::Maximized;
			this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &Form1::Form1_FormClosing);
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->toolStrip1->ResumeLayout(false);
			this->toolStrip1->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: static int counter = 0;
	private: HealthAssessment^	hAssess;
	private: DBConnection ^dbconn;
	private: PartSearch^	searchForm;
	private: ConnectForm^	connForm;
	private: System::Void serviceEntryToolStripMenuItem1_Click(System::Object^  sender, System::EventArgs^  e) {
				if(this->connButton->Enabled==true)
				{
					MessageBox::Show("You must be connected in order to enter new items!");	 
				}
				else
				{
					this->childForm = (gcnew ServiceEntry());
					this->childForm->MdiParent = this;
					counter++;
					this->childForm->SetText(String::Format("This is window #{0}",counter.ToString()));
					this->childForm->Show();
				}
			 }
private: System::Void programParticipantToolStripMenuItem1_Click(System::Object^  sender, System::EventArgs^  e) {
			 if(this->connButton->Enabled==true)
			{
				 MessageBox::Show("You must be connected in order to enter new items!");	 
			}
			else
			{ 
				this->partForm = (gcnew ParticipantEnrollment());
				this->partForm->MdiParent = this;
				//this->partForm->SetIDNumText(counter.ToString());
				this->partForm->dbconn = this->dbconn;
				this->partForm->update = false;
				this->partForm->Show();
			}
		 }
private: System::Void healthAssessmentToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 if(this->connButton->Enabled==true)
			 {
				 MessageBox::Show("You must be connected in order to enter new items!");	 
			 }
			 else
			 {
				this->hAssess = (gcnew HealthAssessment());
				this->hAssess->MdiParent = this;
				this->hAssess->Show();
			}
		 }
private: System::Void toolStripButton1_Click(System::Object^  sender, System::EventArgs^  e) {
			 if(!connForm) {
				 this->connForm = (gcnew ConnectForm());
				 this->connForm->MdiParent = this;
				 //this->connForm->dbconnp = %dbconn;
				 this->connForm->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &Form1::connForm_FormClosing);
				 this->connForm->Show();
			 }
			 
			 //dbconn = gcnew DBConnection();
			 
			 //this->disconnButton->Enabled = true;
			 //this->connButton->Enabled = false;
		 }
private: System::Void disconnButton_Click(System::Object^  sender, System::EventArgs^  e) {
			 dbconn->Close();
			 this->disconnButton->Enabled = false;
			 this->disconnectToolStripMenuItem->Enabled = false;
			 this->connButton->Enabled = true;
			 this->toolStripMenuItem1->Enabled = true;
			 delete dbconn;
		 }
private: System::Void searchButton_Click(System::Object^  sender, System::EventArgs^  e) {
			 if(this->connButton->Enabled==true)
			 {
				 MessageBox::Show("You must be connected in order to search for items!");	 
			 }
			 else
			 {
				 if(!searchForm) {
					this->searchForm = (gcnew PartSearch());
					this->searchForm->MdiParent = this;
					this->searchForm->dbconn = this->dbconn;
					this->searchForm->Show();
					this->searchButton->Enabled = false;
					this->searchForm->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &Form1::searchForm_FormClosing);
				}
			 }
		 }
private: System::Void searchForm_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) {
			 this->searchForm = nullptr;
			 this->searchButton->Enabled = true;
		 }
private: System::Void Form1_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) {
			if(dbconn)	 
				dbconn->Close();
		 }
private: System::Void connForm_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) {
			 if(connForm->dbconn) {
				dbconn = connForm->dbconn;
				this->connButton->Enabled = false;
				this->toolStripMenuItem1->Enabled = false;
				this->disconnButton->Enabled = true;
				this->disconnectToolStripMenuItem->Enabled = true;
			 } else {
				this->connButton->Enabled = true;
				this->toolsToolStripMenuItem->Enabled = true;
				this->disconnButton->Enabled = false;
				this->disconnectToolStripMenuItem->Enabled = true;
			 }
			 this->connForm = nullptr;
		 }
private: System::Void Form1_Load(System::Object^  sender, System::EventArgs^  e) {
		 }
};
}

