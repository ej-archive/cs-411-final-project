#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

//Added by Jenkins
#include "DBConnection.h"
#include "ParticipantEnrollment.h"
#include "HealthAssessment.h"
#include "ServiceEntry.h"
#include "ServiceHistory.h"
#include "ViewChildren.h"

namespace WIC {

	/// <summary>
	/// Summary for PartOptions
	///
	/// WARNING: If you change the name of this class, you will need to change the
	///          'Resource File Name' property for the managed resource compiler tool
	///          associated with all .resx files this class depends on.  Otherwise,
	///          the designers will not be able to interact properly with localized
	///          resources associated with this form.
	/// </summary>
	public ref class PartOptions : public System::Windows::Forms::Form
	{
	public:
		PartOptions(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~PartOptions()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::GroupBox^  groupBox1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::TextBox^  nameText;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::TextBox^  addressText;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::DateTimePicker^  dobField;

	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::TextBox^  typeText;
	private: System::Windows::Forms::GroupBox^  groupBox2;
	private: System::Windows::Forms::Button^  editParticipantButton;
	private: System::Windows::Forms::Button^  serviceEntryButton;


	private: System::Windows::Forms::Button^  healthAssessmentButton;
	private: System::Windows::Forms::Button^  closeButton;
	private: System::Windows::Forms::Button^  serviceHistoryButton;
	private: System::Windows::Forms::Button^  addChildButton;

	protected: 

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->addressText = (gcnew System::Windows::Forms::TextBox());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->dobField = (gcnew System::Windows::Forms::DateTimePicker());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->typeText = (gcnew System::Windows::Forms::TextBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->nameText = (gcnew System::Windows::Forms::TextBox());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->serviceHistoryButton = (gcnew System::Windows::Forms::Button());
			this->addChildButton = (gcnew System::Windows::Forms::Button());
			this->closeButton = (gcnew System::Windows::Forms::Button());
			this->editParticipantButton = (gcnew System::Windows::Forms::Button());
			this->serviceEntryButton = (gcnew System::Windows::Forms::Button());
			this->healthAssessmentButton = (gcnew System::Windows::Forms::Button());
			this->groupBox1->SuspendLayout();
			this->groupBox2->SuspendLayout();
			this->SuspendLayout();
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->addressText);
			this->groupBox1->Controls->Add(this->label4);
			this->groupBox1->Controls->Add(this->dobField);
			this->groupBox1->Controls->Add(this->label3);
			this->groupBox1->Controls->Add(this->typeText);
			this->groupBox1->Controls->Add(this->label2);
			this->groupBox1->Controls->Add(this->nameText);
			this->groupBox1->Controls->Add(this->label1);
			this->groupBox1->Location = System::Drawing::Point(12, 12);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(377, 182);
			this->groupBox1->TabIndex = 0;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"Participant Info";
			// 
			// addressText
			// 
			this->addressText->Location = System::Drawing::Point(9, 91);
			this->addressText->Multiline = true;
			this->addressText->Name = L"addressText";
			this->addressText->ReadOnly = true;
			this->addressText->Size = System::Drawing::Size(195, 74);
			this->addressText->TabIndex = 4;
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(6, 74);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(45, 13);
			this->label4->TabIndex = 6;
			this->label4->Text = L"Address";
			// 
			// dobField
			// 
			this->dobField->CustomFormat = L"yyyy-MM-dd";
			this->dobField->Enabled = false;
			this->dobField->Format = System::Windows::Forms::DateTimePickerFormat::Custom;
			this->dobField->Location = System::Drawing::Point(80, 44);
			this->dobField->Name = L"dobField";
			this->dobField->Size = System::Drawing::Size(87, 20);
			this->dobField->TabIndex = 3;
			this->dobField->Value = System::DateTime(2000, 1, 1, 0, 0, 0, 0);
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(6, 48);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(68, 13);
			this->label3->TabIndex = 4;
			this->label3->Text = L"Date of birth:";
			// 
			// typeText
			// 
			this->typeText->Location = System::Drawing::Point(263, 13);
			this->typeText->Name = L"typeText";
			this->typeText->ReadOnly = true;
			this->typeText->Size = System::Drawing::Size(100, 20);
			this->typeText->TabIndex = 2;
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(223, 16);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(34, 13);
			this->label2->TabIndex = 2;
			this->label2->Text = L"Type:";
			// 
			// nameText
			// 
			this->nameText->Location = System::Drawing::Point(50, 13);
			this->nameText->Name = L"nameText";
			this->nameText->ReadOnly = true;
			this->nameText->Size = System::Drawing::Size(154, 20);
			this->nameText->TabIndex = 1;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(6, 16);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(38, 13);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Name:";
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->serviceHistoryButton);
			this->groupBox2->Controls->Add(this->addChildButton);
			this->groupBox2->Controls->Add(this->closeButton);
			this->groupBox2->Controls->Add(this->editParticipantButton);
			this->groupBox2->Controls->Add(this->serviceEntryButton);
			this->groupBox2->Controls->Add(this->healthAssessmentButton);
			this->groupBox2->Location = System::Drawing::Point(13, 201);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Size = System::Drawing::Size(376, 177);
			this->groupBox2->TabIndex = 1;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"Participant Actions";
			// 
			// serviceHistoryButton
			// 
			this->serviceHistoryButton->Location = System::Drawing::Point(195, 67);
			this->serviceHistoryButton->Name = L"serviceHistoryButton";
			this->serviceHistoryButton->Size = System::Drawing::Size(175, 42);
			this->serviceHistoryButton->TabIndex = 10;
			this->serviceHistoryButton->Text = L"Service History";
			this->serviceHistoryButton->UseVisualStyleBackColor = true;
			this->serviceHistoryButton->Click += gcnew System::EventHandler(this, &PartOptions::serviceHistoryButton_Click);
			// 
			// addChildButton
			// 
			this->addChildButton->Location = System::Drawing::Point(6, 115);
			this->addChildButton->Name = L"addChildButton";
			this->addChildButton->Size = System::Drawing::Size(175, 42);
			this->addChildButton->TabIndex = 9;
			this->addChildButton->Text = L"Add/View Children";
			this->addChildButton->UseVisualStyleBackColor = true;
			this->addChildButton->Click += gcnew System::EventHandler(this, &PartOptions::addChildButton_Click);
			// 
			// closeButton
			// 
			this->closeButton->DialogResult = System::Windows::Forms::DialogResult::Cancel;
			this->closeButton->Location = System::Drawing::Point(195, 115);
			this->closeButton->Name = L"closeButton";
			this->closeButton->Size = System::Drawing::Size(175, 42);
			this->closeButton->TabIndex = 8;
			this->closeButton->Text = L"Close";
			this->closeButton->UseVisualStyleBackColor = true;
			this->closeButton->Click += gcnew System::EventHandler(this, &PartOptions::closeButton_Click);
			// 
			// editParticipantButton
			// 
			this->editParticipantButton->Location = System::Drawing::Point(6, 67);
			this->editParticipantButton->Name = L"editParticipantButton";
			this->editParticipantButton->Size = System::Drawing::Size(175, 42);
			this->editParticipantButton->TabIndex = 7;
			this->editParticipantButton->Text = L"Edit Participant";
			this->editParticipantButton->UseVisualStyleBackColor = true;
			this->editParticipantButton->Click += gcnew System::EventHandler(this, &PartOptions::editParticipantButton_Click);
			// 
			// serviceEntryButton
			// 
			this->serviceEntryButton->Location = System::Drawing::Point(195, 19);
			this->serviceEntryButton->Name = L"serviceEntryButton";
			this->serviceEntryButton->Size = System::Drawing::Size(175, 42);
			this->serviceEntryButton->TabIndex = 6;
			this->serviceEntryButton->Text = L"Create Service Entry";
			this->serviceEntryButton->UseVisualStyleBackColor = true;
			this->serviceEntryButton->Click += gcnew System::EventHandler(this, &PartOptions::serviceEntryButton_Click);
			// 
			// healthAssessmentButton
			// 
			this->healthAssessmentButton->Location = System::Drawing::Point(6, 19);
			this->healthAssessmentButton->Name = L"healthAssessmentButton";
			this->healthAssessmentButton->Size = System::Drawing::Size(175, 42);
			this->healthAssessmentButton->TabIndex = 5;
			this->healthAssessmentButton->Text = L"Create Health Assessment";
			this->healthAssessmentButton->UseVisualStyleBackColor = true;
			this->healthAssessmentButton->Click += gcnew System::EventHandler(this, &PartOptions::healthAssessmentButton_Click);
			// 
			// PartOptions
			// 
			this->AcceptButton = this->closeButton;
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->CancelButton = this->closeButton;
			this->ClientSize = System::Drawing::Size(401, 390);
			this->Controls->Add(this->groupBox2);
			this->Controls->Add(this->groupBox1);
			this->MaximumSize = System::Drawing::Size(409, 417);
			this->Name = L"PartOptions";
			this->Text = L"PartOptions";
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->groupBox2->ResumeLayout(false);
			this->ResumeLayout(false);

		}
#pragma endregion
	public: DBConnection ^dbconn;
	public: bool update;
	private: DataGridViewTextBoxCell^ assIDCell;
	private: String^ id;
	private: IEnumerator^ current_enum;
	private: ParticipantEnrollment^ partForm;
	private: ServiceHistory^ histForm;
	private: ViewChildren^ childForm;
	public: void Update(System::Collections::IEnumerator^ my_enum,String^ type){
			//String^ tmp;
			update = true;
			current_enum = my_enum;
			this->Text = "Participant Options";
			this->typeText->Text = type;
			my_enum->MoveNext();
			DataGridViewTextBoxCell^ my_cell = (cli::safe_cast<DataGridViewTextBoxCell ^>(my_enum->Current));
			id = my_cell->Value->ToString();
			my_enum->MoveNext();
			my_cell = (cli::safe_cast<DataGridViewTextBoxCell ^>(my_enum->Current));
			this->nameText->Text = (my_cell->Value)->ToString();
			my_enum->MoveNext();
			my_cell = (cli::safe_cast<DataGridViewTextBoxCell ^>(my_enum->Current));
			this->addressText->Text = (my_cell->Value)->ToString();
			my_enum->MoveNext();
			my_cell = (cli::safe_cast<DataGridViewTextBoxCell ^>(my_enum->Current));
			int pos = 0;
			String  ^dob = my_cell->Value->ToString();
			String ^month = dob->Substring(pos,2);
			if(month[1] == '/') {
				month = month->Substring(0,1);
				pos += 2;
			} else {
				pos += 3;
			}
			String ^day = dob->Substring(pos,2);
			if(day[1] == '/') {
				day = day->Substring(0,1);
				pos += 2;
			} else {
				pos += 3;
			}
			String ^year = dob->Substring(pos,4);
			this->dobField->Value = System::DateTime(Int32::Parse(year),Int32::Parse(month),Int32::Parse(day));
			if(type=="Mother" || type=="Adult")
			{
				for(int i = 0; i<7; i++)
					my_enum->MoveNext();
			}
			else
			{
				for(int i = 0; i<4; i++)
					my_enum->MoveNext();
				this->addChildButton->Enabled=false;
			}
			assIDCell = (cli::safe_cast<DataGridViewTextBoxCell ^>(my_enum->Current));
			if(assIDCell->Value->ToString() != "")
			{
				this->healthAssessmentButton->Text = "Edit Health Assessment";
			}
		 }
private: System::Void healthAssessmentButton_Click(System::Object^  sender, System::EventArgs^  e) {
			 HealthAssessment^ healthForm = gcnew HealthAssessment();
			 healthForm->MdiParent = this->MdiParent;
			 healthForm->dbconn = this->dbconn;
			 healthForm->FormClosed += gcnew System::Windows::Forms::FormClosedEventHandler(this, &PartOptions::enableHealthAssButton);
			 this->healthAssessmentButton->Enabled = false;
			 if(this->healthAssessmentButton->Text=="Create Health Assessment")
			 {
				 healthForm->update = false;
				 healthForm->okButton->Click += gcnew System::EventHandler(this, &PartOptions::changeButtonText);
			 } else {
				 healthForm->update = true;
			 }
			 healthForm->Update(this->id, this->nameText->Text, this->typeText->Text, this->dobField, this->assIDCell);
			 healthForm->Show();
		 }
private: System::Void editParticipantButton_Click(System::Object^  sender, System::EventArgs^  e) {
			 partForm = gcnew ParticipantEnrollment();
			 partForm->MdiParent = this->MdiParent;
			 partForm->dbconn = this->dbconn;
			 current_enum->Reset();
			 partForm->Update(current_enum,this->typeText->Text,this->dobField,this->nameText,this->addressText);
			 this->editParticipantButton->Enabled = false;
			 partForm->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &PartOptions::enableEditPartButton);
			 //partForm->okButton->Click += gcnew System::EventHandler(this, &PartOptions::updateFields);
			 partForm->Show();
		 }
private: System::Void serviceEntryButton_Click(System::Object^  sender, System::EventArgs^  e) {
			 ServiceEntry^ servForm = gcnew ServiceEntry();
			 servForm->MdiParent = this->MdiParent;
			 servForm->dbconn = this->dbconn;
			 servForm->FormClosed += gcnew System::Windows::Forms::FormClosedEventHandler(this, &PartOptions::enableServiceEntryButton);
			 this->serviceEntryButton->Enabled = false;
			 servForm->Update(this->id, this->nameText->Text, this->typeText->Text, this->dobField);
			 servForm->Show();
		 }
private: System::Void closeButton_Click(System::Object^  sender, System::EventArgs^  e) {
			 this->Close();
		 }
private: System::Void changeButtonText(System::Object^  sender, System::EventArgs^  e) {
			 this->healthAssessmentButton->Text = "Edit Health Assessment";

		 }
private: System::Void enableEditPartButton(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) {
			 this->editParticipantButton->Enabled = true;
		 }
private: System::Void enableHealthAssButton(System::Object^  sender, System::Windows::Forms::FormClosedEventArgs^  e) {
			 this->healthAssessmentButton->Enabled = true;
		 }
private: System::Void enableServiceEntryButton(System::Object^  sender, System::Windows::Forms::FormClosedEventArgs^  e) {
			 this->serviceEntryButton->Enabled = true;
		 }
private: System::Void updateFields(System::Object^  sender, System::EventArgs^  e) {
			 this->nameText->Text = partForm->nameText->Text;
			 this->addressText->Text = partForm->addressText->Text;
			 this->dobField->Value = partForm->dobField->Value;
		 }
private: System::Void serviceHistoryButton_Click(System::Object^  sender, System::EventArgs^  e) {
			 histForm = gcnew ServiceHistory();
			 histForm->MdiParent = this->MdiParent;
			 histForm->dbconn = this->dbconn;
			 histForm->FormClosed += gcnew System::Windows::Forms::FormClosedEventHandler(this, &PartOptions::enableServiceHistoryButton);
			 this->serviceHistoryButton->Enabled = false;
			 if(typeText->Text == "Mother")
				 dbconn->Search(histForm->searchView,
				 "ServiceEntry e, AdultReceives r",
				 "r.AdultID = "+ id +" AND e.EntryID = r.EntryID","e.*");
			 else
				 dbconn->Search(histForm->searchView,
				 "ServiceEntry e, ChildReceives r",
				 "r.ChildID = "+ id +" AND e.EntryID = r.EntryID","e.*");
			 histForm->Show();
		 }
private: System::Void enableServiceHistoryButton(System::Object^  sender, System::Windows::Forms::FormClosedEventArgs^  e) {
			 this->serviceHistoryButton->Enabled = true;
		 }
private: System::Void addChildButton_Click(System::Object^  sender, System::EventArgs^  e) {
			 childForm = gcnew ViewChildren();
			 childForm->MdiParent = this->MdiParent;
			 childForm->dbconn = this->dbconn;
			 childForm->parentID = this->id;
			 childForm->FormClosed += gcnew System::Windows::Forms::FormClosedEventHandler(this, &PartOptions::enableAddChildButton);
			 this->addChildButton->Enabled = false;
			 dbconn->Search(childForm->searchView, "Child","ParentID = " + id, "*");
			 childForm->Show();
		 }
 private: System::Void enableAddChildButton(System::Object^  sender, System::Windows::Forms::FormClosedEventArgs^  e) {
			 this->addChildButton->Enabled = true;
		 }
};
}


