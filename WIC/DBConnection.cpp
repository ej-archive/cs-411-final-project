#include "stdafx.h"
#include "DBConnection.h"

DBConnection::DBConnection(String ^server,String ^uname,String ^passwd)
{
	myConnection = gcnew MySqlConnection(String::Format("Server={0};Database=WIC;Uid={1};Pwd={2};",
		server,uname,passwd));
	myConnection->Open();
}

void DBConnection::Insert(String ^table,String ^tuple)
{
	if(table == "Mother") table = "Adult";
	MySqlCommand ^myCommand;
	myCommand = gcnew MySqlCommand("INSERT INTO " + table + " VALUES " + tuple,myConnection);
	MessageBox::Show("INSERT INTO " + table + " VALUES " + tuple);
	
	try{
		myCommand->ExecuteNonQuery();
	}
	catch(MySqlException^ ex)
	{
		MessageBox::Show("Invalid SQL syntax: " + ex->Message);
	}

}

void DBConnection::Close()
{
	myConnection->Close();
}

DBConnection::~DBConnection()
{
	delete myConnection;
}

void DBConnection::Search(String^ table, String^ searchTerm,String^ searchField,DataGridView^ dataGrid)
{
	data = gcnew DataTable();

	String^ query = String::Format("SELECT * FROM {0} a WHERE a.{1} LIKE '%{2}%'",
		table,searchField,searchTerm);

	MessageBox::Show("Running query...\n" + query);
			
	da = gcnew MySqlDataAdapter(query, myConnection );
	cb = gcnew MySqlCommandBuilder( da );

	da->Fill( data );

	dataGrid->DataSource = (cli::safe_cast<System::Object^ > (data));
	
}

void DBConnection::Update()
{
	DataTable^ changes = data->GetChanges();
	if(changes != nullptr) {
		da->Update( changes );
		data->AcceptChanges();
	}
}

void DBConnection::RunNonQuery(String ^query) 
{
	MySqlCommand ^myCommand;
	myCommand = gcnew MySqlCommand(query,myConnection);
	MessageBox::Show(query);
	
	try{
		myCommand->ExecuteNonQuery();
	}
	catch(MySqlException^ ex)
	{
		MessageBox::Show("Invalid SQL syntax: " + ex->Message);
	}
}

String^ DBConnection::Get(String^ table, String^ field, String^ id)
{
	if(table == "Mother") table = "Adult";
	MySqlCommand ^myCommand;
	MySqlDataReader ^reader = nullptr;
	String ^retval = nullptr;
	String^ query = ("SELECT " + field + " FROM " + table + " WHERE AssessmentID = " + id);

	MessageBox::Show("Running query...\n" + query);
	myCommand = gcnew MySqlCommand(query, myConnection);
	try {
		reader = myCommand->ExecuteReader();
		reader->Read();
		retval = reader->GetString(0);
	}
	catch (MySqlException ^ex) 
	{
		MessageBox::Show("Invalid SQL syntax: " + ex->Message);
	}
	finally 
	{
		if (reader) reader->Close();
	}
	return retval;
}

cli::array<String^> ^DBConnection::Get(String^ table, String ^idName,String^ id,int size)
{
	if(table == "Mother") table = "Adult";
	MySqlCommand ^myCommand;
	MySqlDataReader ^reader = nullptr;
	cli::array<String ^> ^retval = nullptr;
	String^ query = ("SELECT * FROM " + table + " WHERE " + idName + " = " + id);

	MessageBox::Show("Running query...\n" + query);
	myCommand = gcnew MySqlCommand(query, myConnection);
	try {
		reader = myCommand->ExecuteReader();
		int i;
		
		retval = gcnew cli::array<String ^> (size);
		reader->Read();
		for(i=0;i<size;i++)
			retval[i] = reader->GetString(i);
	}
	catch (MySqlException ^ex) 
	{
		MessageBox::Show("Invalid SQL syntax: " + ex->Message);
	}
	finally 
	{
		if (reader) reader->Close();
	}
	return retval;
}

String ^DBConnection::GetID(String^ table,String^ idName)
{
	MySqlCommand ^myCommand;
	MySqlDataReader ^reader = nullptr;

	String^ query = ("SELECT MAX("+ idName +") FROM " + table);
	String^ retval;
	//MessageBox::Show("Running query...\n" + query);
	myCommand = gcnew MySqlCommand(query, myConnection);
	try {
		reader = myCommand->ExecuteReader();
		if(!reader)
			return nullptr;
		reader->Read();
		if(!reader)
			return nullptr;
		retval = reader->GetString(0);
	}
	catch (MySqlException ^ex) 
	{
		MessageBox::Show("Invalid SQL syntax: " + ex->Message);
	}
	finally 
	{
		if (reader) reader->Close();
	}
	return retval;
}

void DBConnection::Search(DataGridView^ dataGrid,String^ table, String^ condition,String^ values)
{
	data = gcnew DataTable();

	String^ query = String::Format("SELECT {0} FROM {1} WHERE {2}",
		values,table,condition);

	MessageBox::Show("Running query...\n" + query);
			
	da = gcnew MySqlDataAdapter(query, myConnection );
	cb = gcnew MySqlCommandBuilder( da );

	da->Fill( data );

	dataGrid->DataSource = (cli::safe_cast<System::Object^ > (data));
	
}